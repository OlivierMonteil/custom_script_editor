"""
This file should be run at Maya startup. If not, please add ./src folder to
Maya.env's PYTHONPATH so this file will be detected by Maya at startup.
"""

from maya import cmds
from custom_script_editor import main as cse_main

cmds.evalDeferred(cse_main.run, lowestPriority=True)
