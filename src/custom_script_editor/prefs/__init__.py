"""
Maya Script Editor's customization Preferences management files.
"""

# shorten absolute imports
from custom_script_editor.prefs.data import PreferencesManager
