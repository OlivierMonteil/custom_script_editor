"""
Preferences, palette and profile data management script.
"""

import os
import re
import copy

import json

from PySide2 import QtCore

from custom_script_editor import constants as kk


# get default config file
DEFAULT_CONFIG = os.path.join(os.path.dirname(__file__), '.default_prefs')
# get user config file
USER_ROOT = os.path.join(
    os.path.expanduser(os.getenv('USERPROFILE')), '.custom_script_editor'
)


class PreferencesManager(QtCore.QObject):
    """
    Config management class. Allows getting/setting configs from user file and
    save it.
    """

    changed = QtCore.Signal()

    def __init__(self, config_file='.user_prefs', parent=None):
        super(PreferencesManager, self).__init__(parent)

        self.file = os.path.join(USER_ROOT, config_file)

        self.data = self.read()
        self._startup_data = copy.deepcopy(self.data)   # copy for modifications check

    def read(self, default=False):
        """
        Get config data from user file (or default's if not existing yet).
        """

        config_path = self.file if os.path.isfile(self.file) else \
                      DEFAULT_CONFIG

        if default:
            config_path = DEFAULT_CONFIG

        with open(config_path, 'r') as config_file:
            return json.load(config_file)

    def save(self):
        """ Save current data into user config file. """

        # emit signal so manager might update properly before every attribute is
        # set as non-dirty
        self.changed.emit()

        # make sure folder exists
        if not os.path.isdir(USER_ROOT):
            os.mkdir(USER_ROOT)

        # save file
        with open(self.file, 'w') as config_file:
            config_file.write(
                json.dumps(self.data, indent=4, separators=(',', ': '))
            )

        print (kk.SUCCESS_MESSAGE.format('Preferences saved.'))

        # update startup data from data so everything will be set as non-dirty
        self._startup_data = copy.deepcopy(self.data)

    def get_palette_data(self, rule):
        """
        Args:
            rule (str)

        Returns:
            (dict)

        Get palette's data from <rule>'s associated theme.
        """

        theme = self.get_by_names(rule, 'palette')
        return get_palette_data(rule, theme)

    def get_profile_data(self, rule):
        """
        Args:
            rule (str)

        Returns:
            (dict)

        Set profile's data from <rule>'s associated profile.
        """

        profile = self.get_by_names(rule, 'profile')
        return get_profile_data(rule, profile)

    def get_by_names(self, rule, attr, from_dict=None):
        """
        Args:
            rule (str)
            attr (str)

        Get <attr> value from <rule> dict (nested dicts must be separated by a
        '|' character).
        """

        data_dict = self.data if from_dict is None else from_dict

        for x in rule.split('|'):
            data_dict = data_dict[x]

        return data_dict[attr]

    def set_by_names(self, rule, attr, value):
        """
        Args:
            rule (str)
            attr (str)
            value (object)

        Returns:
            (object)

        Set <attr> value from <rule> dict (nested dicts must be separated by a
        '|' character).
        """

        data_dict = self.data

        for x in rule.split('|'):
            data_dict = data_dict[x]

        # convert 'None' profiles to null value into json dicts
        if value == 'None':
            value = None

        data_dict[attr] = value

    def is_dirty(self):
        """ Returns: (bool) """

        if self.data != self._startup_data:
            return True
        return False

    def is_attr_dirty(self, rule, attr):
        """
        Args:
            rule (str)
            attr (str)

        Returns:
            (bool)

        Get dirty state for scpecific <attr>.
        """

        value = self.get_by_names(rule, attr)
        startup_value = self.get_by_names(rule, attr, from_dict=self._startup_data)

        if value != startup_value:
            return True
        return False

    def __str__(self):
        """ Get the formatted string result for this instance. """

        content = json.dumps(self.data, indent=4, separators=(',', ': '))

        width = max([len(line) for line in content.split('\n')]) +6
        title = 'Preferences'
        title_spacing = ' '*int((width -len(title)-2)/2)
        line_break = '# ' +'-'*width +'\n'

        str_txt  = line_break
        str_txt +=  '# {}{}{}\n'.format(title_spacing, title, title_spacing)
        str_txt +=  '#\n'

        for line in content.split('\n'):
            str_txt += '#   {}{}\n'.format(line, ' '*int(width -4 -len(line)))

        str_txt += line_break

        return str_txt


def get_all_json_from_dir(directory, short=False):
    """
    Args:
        directory (str) : absolute path
        short (bool, optional)

    Returns:
        (list[str])

    Recursively get all the json files under <directory>. Sub-folders which start
    with a '.' character will be skipped from recursion. Files called 'template.json'
    will also be expluded from the result.
    If <short>, short names will be returned instead of absolute path.
    """

    result = []

    if not os.path.isdir(directory):
        return result

    for item in os.listdir(directory):
        if item.startswith('.') or item == 'template.json':
            continue

        path = os.path.join(directory, item)

        if os.path.isfile(path) and path.endswith('.json'):
            if short:
                result.append(item[:-5])
            else:
                result.append(path)

        # recurse
        elif os.path.isdir(path):
            result.extend(get_all_json_from_dir(path, short=short))

    return result

def get_themes_list(rule, short=True):
    """
    Args:
        rule (str)
        short (bool, optional)

    Returns:
        (list[str])

    Get highlights presets list for specific <rule>.
    """

    rule = to_parent_rule_name(rule)
    root_path = os.path.join(kk.PALETTES_ROOT, rule)

    return get_all_json_from_dir(root_path, short=short)

def get_profiles_list(rule, short=True):
    """
    Args:
        rule (str)
        short (bool, optional)

    Returns:
        (list[str])

    Get profiles list for specific <rule> (adding 'None', 'Full' and 'Off' to
    the list).
    """

    rule = to_parent_rule_name(rule)

    profiles = [kk.PROFILE_NONE, kk.PROFILE_FULL, kk.PROFILE_OFF]
    root_path = os.path.join(kk.PALETTES_ROOT, rule, kk.PROFILES_DIR)

    return profiles +get_all_json_from_dir(root_path, short=short)

def get_attributes_list(rule):
    """
    Args:
        rule (str)

    Returns:
        (list[str])

    Get attributes list from <rule>'s according template.json file.
    """

    return get_template_data(rule)['attributes'].keys()

def get_palette_data(rule, theme):
    """
    Args:
        rule (str)
        theme (str)

    Returns:
        (dict)

    Get the <theme> palette data from the according json file.
    """

    palette_file = os.path.join(
        kk.PALETTES_ROOT, to_parent_rule_name(rule), '{}.json'.format(theme)
    )

    with open(palette_file, 'r') as opened_file:
        return json.load(opened_file)

def get_profile_data(rule, profile):
    """
    Args:
        rule (str)
        profile (str)

    Get the <name> profile data from the according json file.
    """

    # return a full {"key": True/False} dict for Full/Off profiles (except for
    # default attributes as "background" and "normal")
    if profile in ('Full', 'Off'):
        value = True if profile == 'Full' else False
        value_dict = {}

        for x in get_attributes_list(rule):
            if x not in ('background', 'normal'):
                value_dict[x] = value

        return value_dict

    # return a full {"key": None} dict for Full/Off profile (except for default
    # attributes as "background" and "normal"). None values will be skipped at
    # profile's resolution and "enabled" values from palette data will stay untouched
    if profile in ('None', None):
        value_dict = {}

        for x in get_attributes_list(rule):
            if x not in ('background', 'normal'):
                value_dict[x] = None

        return value_dict

    # else, get profile's json path and return its content
    profile_file = os.path.join(
        kk.PALETTES_ROOT,
        to_parent_rule_name(rule),
        kk.PROFILES_DIR,
        '{}.json'.format(profile)
    )

    with open(profile_file, 'r') as opened_file:
        return json.load(opened_file)

def get_template_data(rule):
    """
    Args:
        rule (str)

    Returns:
        (dict)

    Get data from <rule>'s according template.json file.
    """

    template_path = os.path.join(
        kk.PALETTES_ROOT, to_parent_rule_name(rule), 'template.json'
    )

    with open(template_path, 'r') as profile_file:
        return json.load(profile_file)

def save_to_file(path, data_dict):
    """
    Args:
        path (str) : absolute path
        data_dict (dict)

    Save <data_dict> into json <path>.
    """

    content = json.dumps(data_dict, indent=4, separators=(',', ': '), sort_keys=True)
    # remove line breaks on lists
    content = re.sub(
        r'\[\s+(\d+),\s+(\d+),\s+(\d+)\s+\]',
        '[\g<1>, \g<2>, \g<3>]',
        content
    )

    with open(path, 'w') as opened_file:
        opened_file.write(content)

def to_parent_rule_name(rule):
    """
    Args:
        rule (str)

    Convert nested rules (like "console|python-highlight") into child's rule
    (in this case, "python").
    """

    if '|' in rule and 'mel' in rule:
        rule = 'mel'
    elif '|' in rule and 'python' in rule:
        rule = 'python'

    return rule
