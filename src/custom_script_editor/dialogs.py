"""
Dialogs classes and functions.
"""

import os
import re

from PySide2 import QtWidgets, QtCore, QtGui


SAVE = QtWidgets.QMessageBox.StandardButton.Save
CANCEL = QtWidgets.QMessageBox.StandardButton.Cancel
RESERVED_PROFILES = ['Off', 'Full', 'None']


class SaveDialog(QtWidgets.QDialog):
    """
    Palette Editor's saving dialog (for profiles or palettes).
    """

    def __init__(self, files_type, root_dir, title=None, selected=None, parent=None):
        super(SaveDialog, self).__init__(parent)

        self._files_type = files_type

        if title is None:
            title = 'Save {} as...'.format(files_type.capitalize())

        self.setWindowTitle(title)

        layout = QtWidgets.QGridLayout(self)

        # root path widgets
        path_title = QtWidgets.QLabel('Look in :', self)
        self._path_box = RootBox(root_dir, self)

        # theme's view
        self._view = View(root_dir, self)

        # file names widgets
        file_title = QtWidgets.QLabel('File name :', self)
        self._name_field = QtWidgets.QLineEdit(
            self,
            placeholderText='select or enter theme\'s name...'
        )

        # file type widgets
        type_title = QtWidgets.QLabel('Files of type :', self)
        type_box = TypeBox(['JSON files   (*.json)'], self)

        # main Save/Cancel buttons
        buttons_widget = QtWidgets.QWidget(self)
        buttons_lay = QtWidgets.QHBoxLayout(buttons_widget)

        self._save_button = QtWidgets.QPushButton('Save', self, enabled=False)
        cancel_button = QtWidgets.QPushButton('Cancel', self)

        # populate layouts
        layout.addWidget(path_title, 0, 0)
        layout.addWidget(self._path_box, 0, 1)
        layout.addWidget(self._view, 1, 0, 1, 2)
        layout.addWidget(file_title, 2, 0)
        layout.addWidget(self._name_field, 2, 1)
        layout.addWidget(type_title, 3, 0)
        layout.addWidget(type_box, 3, 1)
        layout.addWidget(buttons_widget, 4, 0, 1, 2)
        buttons_lay.addWidget(self._save_button)
        buttons_lay.addWidget(cancel_button)

        # add custom validator on theme names field
        self._name_field.setValidator(FileNamesValidator(self._name_field))
        self._name_field.textChanged.connect(self._on_name_changed)
        self._name_field.returnPressed.connect(self.accept)

        # connect signals
        self._view.doubleClicked.connect(self._on_view_double_clicked)
        self._view.item_clicked.connect(self._on_view_clicked)
        # main buttons signals
        self._save_button.clicked.connect(self.accept)
        cancel_button.clicked.connect(self.reject)

        # few size rules
        layout.setColumnStretch(1, 1)
        for widget in (self, buttons_widget, buttons_lay):
            widget.setContentsMargins(0, 0, 0, 0)

        # set selected theme if set
        if selected:
            self._select(selected)

        # set focus on theme field and select text
        self._name_field.selectedText()
        self._name_field.setFocus()

    def get_selected_path(self):
        """
        Returns:
            (str) : absolute path

        Get current them's path from QLineEdit (if field is empty, None will be
        returned).
        """

        root = self._view.get_root()
        theme = self._get_name()

        if not theme:
            return None

        if not theme.endswith('.json'):
            theme += '.json'

        return os.path.join(root, theme).replace('\\', '/')

    def accept(self):
        """
        Qt re-implementation. Check a few conditions before validating the dialog.
        """

        short_name = self._get_short_name()
        reserved_match = self._reserved_name_match(short_name)

        if reserved_match:
            message = '"{}" is a reserved names for {}s.\nPlease enter a different name.'
            display_warning(
                message.format(reserved_match, self._files_type),
                parent=self
            )
            return

        super(SaveDialog, self).accept()

    def _get_short_name(self):
        """
        Returns:
            (str)

        Get file's short name from QLineEdit.
        """

        return self._get_name().split('.json')[0]

    def _get_name(self):
        """
        Returns:
            (str)

        Get file's name from QLineEdit (adding extension if not in field).
        """

        file_name = self._name_field.text()
        return file_name if file_name.endswith('.json') else file_name +'.json'

    def _reserved_name_match(self, name):
        """
        Args:
            name (str)

        Returns:
            (str or None)

        Return reserved name match, case-permitive (some names are reserved, as
        "Full" for profile names, and need to be forbidden from file saving).
        If the name is not valid, return None.
        """

        if self._files_type == 'profile':
            for x in RESERVED_PROFILES:
                if name.lower() == x.lower():
                    return x

        return None

    def _on_view_clicked(self, path):
        """
        Args:
            path (str)

        Update name field and Save button's enabled state on View clicks (whether
        an item is selected or not).
        """

        # no valid item cliked
        if not path or not os.path.isfile(path):
            self._save_button.setEnabled(False)
            self._view.clearSelection()
            self._name_field.setText('')

        else:
            self._save_button.setEnabled(True)
            self._name_field.setText(self._view.get_current_theme_name())

    def _on_view_double_clicked(self, *args):
        """ Validate dialog on double-click. """
        self.accept()

    def _on_name_changed(self, txt):
        """
        Args:
            txt (str)

        Update Save button's enabled state and view's selection on name change.
        """

        if txt:
            self._save_button.setEnabled(True)
        else:
            self._save_button.setEnabled(False)

        self._view.update_selection(txt)

    def _select(self, path):
        """
        Args:
            path (str)

        Set input <path> selected in view and update name's field (Save button
        will also be set as enabled).
        """

        if self._view.select(path):
            name = path.replace('\\', '/').split('/')[-1].split('.')[0]
            self._name_field.setText(name)


class TypeBox(QtWidgets.QComboBox):
    """
    File types QComboBox.
    """

    def __init__(self, types, parent=None):
        super(TypeBox, self).__init__(parent)

        for t in types:
            self.addItem(t)

class RootBox(QtWidgets.QComboBox):
    """
    Root directory's QComboBox, with popup diabled and "Copy" action added on
    RMB click.
    """

    def __init__(self, root, parent=None):
        super(RootBox, self).__init__(parent)

        # add root as single item
        self.addItem(root)
        # disable popup
        self.showPopup = lambda: None

    def contextMenuEvent(self, event):
        """
        Qt re-implementation, adds "Copy" action on RMB click.
        """

        menu = QtWidgets.QMenu(self)
        copy_action = menu.addAction('Copy')

        # run menu
        action = menu.exec_(self.mapToGlobal(event.pos()))

        if action == copy_action:
            clipboard = QtWidgets.qApp.clipboard()
            clipboard.clear()
            clipboard.setText(self.currentText())


class View(QtWidgets.QListView):
    """
    Palette themes (.json files) view.
    """

    item_clicked = QtCore.Signal(object)

    def __init__(self, root_dir, parent):
        super(View, self).__init__(
            parent,
            selectionMode=QtWidgets.QAbstractItemView.SingleSelection
        )

        self._root_dir = root_dir

        self.setAlternatingRowColors(True)

        # set file and proxy modle (used to exclude template.json and non-json
        # files from the list
        self._file_model = QtWidgets.QFileSystemModel(self)
        self._file_model.setFilter(QtCore.QDir.Files | QtCore.QDir.NoDotAndDotDot)
        self._proxy_model = ProxyModel(self, root_dir)

        self.setModel(self._proxy_model)
        self._proxy_model.setSourceModel(self._file_model)

        # set root diorectory
        self.set_root(root_dir)

    def get_path(self, proxy_index):
        """
        Args:
            proxy_index (QtCore.QModelIndex)

        Returns:
            (str) : absolute path

        Get path from <proxy_index>.
        """

        model_index = self._proxy_model.mapToSource(proxy_index)
        return self._file_model.filePath(model_index).replace('\\', '/')

    def get_current_theme_name(self):
        """
        Returns:
            (str) : absolute path

        Get view's selected theme's name (without extension).
        """

        idx = self.currentIndex()
        if not idx or not idx.isValid():
            return None

        return self.get_path(idx).split('/')[-1].split('.json')[0]

    def get_root(self):
        """
        Returns:
            (str) : absolute path

        Get view's root path.
        """

        return self._file_model.rootPath()

    def _proxy_index(self, path):
        """
        Args:
            path (str) : absolute path

        Returns:
            (QtCore.QModelIndex) : index from ProxyModel

        Get ProxyModel's index for <path>.
        """

        index = self._file_model.index(path)
        return self._proxy_model.mapFromSource(index)

    def set_root(self, path):
        """
        Args:
            path (str) : absolute path

        Set view's root <path>.
        """

        self._file_model.setRootPath(path)
        self.setRootIndex(self._proxy_index(path))


    def select(self, name):
        """
        Args:
            name (str)

        Returns:
            (bool) : whether <name> was found into view

        Select <name> in view.
        """

        path = os.path.join(self._root_dir, '{}.json'.format(name))
        index = self._proxy_index(path)

        if not index.isValid():
            return False

        select_model = self.selectionModel()
        select_model.select(index, select_model.ClearAndSelect)

        return True

    def update_selection(self, name):
        """
        Args:
            name (str)

        Update view's selection on name's field changes (clear selection if
        current text does not match any theme from the view, else select it).
        """

        self.clearSelection()

        row = 0

        while True:
            idx = self.model().index(row, 0, self.rootIndex())
            if not idx.isValid():
                break

            idx_data = self.model().data(idx)

            name_tokens = name.split('.')
            data_tokens = idx_data.split('.')

            if data_tokens[0] == name_tokens[0]:
                if len(name_tokens) == 1 or \
                   len(name_tokens) > 1 and 'json'.startswith(name_tokens[1]):
                    self.select(name.split('.')[0])
                    break

            row += 1

    def mousePressEvent(self, event):
        """
        Qt re-implementation, emit item_clicked QtCore.Signal() whether a theme
        was selected or not.
        """

        proxy_index = self.indexAt(event.pos())

        if not proxy_index or not proxy_index.isValid():
            self.item_clicked.emit(None)
        else:
            self.item_clicked.emit(self.get_path(proxy_index))

        super(View, self).mousePressEvent(event)

    def keyPressEvent(self, event):
        """
        Qt re-implementation, emit item_clicked QtCore.Signal() on UP/DOWN keys
        so name's field will be updated.
        """

        super(View, self).keyPressEvent(event)

        if event.key() in (QtCore.Qt.Key_Down, QtCore.Qt.Key_Up):
            path = self.get_path(self.currentIndex())
            self.item_clicked.emit(path)


class ProxyModel(QtCore.QSortFilterProxyModel):
    def __init__(self, view, root_dir, *args, **kwargs):
        super(ProxyModel, self).__init__(view, *args, **kwargs)

        self.root_dir = root_dir

    def filterAcceptsRow(self, row, parent_index):
        """
        Args:
            row (int)
            parent_index (QtCore.QModelIndex) : from FileModel !!

        Returns:
            (bool)

        Qt re-implementation. Exclude template.json and non-json files from view.
        """

        source_model = self.sourceModel()
        model_index = parent_index.child(row, 0)
        file_path = source_model.filePath(model_index).replace('\\', '/')

        if os.path.isfile(file_path) and not file_path.endswith('.json'):
            return False

        if file_path.split('/')[-1] == 'template.json':
            return False

        return True


class FileNamesValidator(QtGui.QValidator):
    """
    Files names validator.
    """

    regex_str = '[a-zA-Z0-9_\-\.]*'

    def __init__(self, line_edit):
        super(FileNamesValidator, self).__init__(line_edit)

        self._line_edit = line_edit

    def validate(self, text, pos):
        """
        Args:
            text (str)
            pos (int)

        Returns:
            (tuple(QtGui.QValidator.State, str, int))

        Qt re-implementation, check the valid state of the current <text>
        """

        if not text:
            return QtGui.QValidator.Intermediate, text, pos

        invalid = False

        new_text = ''

        for char in text:
            correction = self._auto_correct(char)

            if not correction is None:
                new_text += correction

            else:
                # add characters that match regString
                if re.match(self.regex_str, char):
                    new_text += char

                else:
                    # else set as temporary
                    invalid = True

        if invalid:
            return QtGui.QValidator.Invalid, text, pos

        # validate if all characters match regString
        return QtGui.QValidator.Acceptable, new_text, pos

    def _auto_correct(self, char):
        """
        Args:
            char (str) : single character

        Returns:
            (str or None) : the corrected character or None

        Get auto-correction on whitespaces or special characters.
        """

        char = char.encode('utf-8')
        bytesChar = bytes(char)

        if bytesChar in [b'\xc3\xa9', b'\xc3\xa8', b'\xc3\xaa', b'\xc3\xab']:
            return 'e'

        if bytesChar in [b'\xc3\xa0', b'\xc3\xa4', b'\xc3\xa2']:
            return 'a'

        if bytesChar in [b'\xc3\xbb', b'\xc3\xbc', b'\xc3\xb9']:
            return 'u'

        if bytesChar in [b'\xc3\xb4', b'\xc3\xb6']:
            return 'o'

        if bytesChar in [b'\xc3\xae', b'\xc3\xaf']:
            return 'i'

        if bytesChar in [b'\xc3\xa7']:
            return 'c'

        if char in [' ']:
            return '_'

        return None


def save_before_close_prompt(parent=None):
    """
    Args:
        parent (QtWidgets.QWidget, optional)

    Returns:
        (QtWidgets.QMessageBox.StandardButton or None)

    Prompt user for modifications save confirmation.
    """

    answer = QtWidgets.QMessageBox.question(
        parent,
        'Save modifications?',
        'Some attributes have been modified.\nDo you want to save them before quitting?',
        buttons = QtWidgets.QMessageBox.Save |
                  QtWidgets.QMessageBox.Discard |
                  QtWidgets.QMessageBox.Cancel
    )

    return answer if answer in (SAVE, CANCEL) else None

def display_warning(message, parent=None):
    """
    Args:
        message (str)
        parent (QtWidgets.QWidget, optional)

    Returns:
        (QtWidgets.QMessageBox.StandardButton or None)

    Show warning dialog with input <message>.
    """

    QtWidgets.QMessageBox.warning(
        parent,
        'Warning',
        message,
        buttons = QtWidgets.QMessageBox.Ok
    )

def highlights_update_prompt(rules, parent=None):
    """
    Args:
        rules (list[str])
        parent (QtWidgets.QWidget, optional)

    Returns:
        (QtWidgets.QMessageBox.StandardButton or None)

    Prompt user for re-highlight on preferences change.
    """

    msg  = 'The following highlights have been modified:\n\n'
    msg += '\n'.join(['  - {}'.format(rule) for rule in rules])
    msg += '\n\n'
    msg += 'Refreshing these highlights might take a while,\n'
    msg += 'do you want to re-highlight them anyway?'

    answer = QtWidgets.QMessageBox.question(
        parent,
        'Re-highlight?',
        msg,
        buttons = QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No
    )

    return True if answer is QtWidgets.QMessageBox.Yes else False

def get_save_name(files_type, root_dir=None, selected=None, parent=None):
    """
    Args:
        files_type (str) : 'theme' or 'profile'
        root_dir (str, optional) : absolute path
        selected (str, optional) : theme's name to be selected at dialog.show()
        parent (QtWidgets.QWidget, optional)

    Returns:
        (str or None)

    Get theme saving file's path.
    """

    dialog = SaveDialog(files_type, root_dir, selected=selected, parent=parent)

    if dialog.exec_():
        return dialog.get_selected_path()
