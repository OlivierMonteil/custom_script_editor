"""
Objects inspection module. Allows actions like getting dir() list or object's
source file, managing temporary imports for required modules.
"""

import sys


SKIPPED = ['test']


def skipped_from_list(func):
    def wrap(obj, *args, **kwargs):
        if obj.as_string() in SKIPPED:
            return None
        return func(obj, *args, **kwargs)
    return wrap


class temporary_import(object):
    """
    Context manager for objects inspection, makes sure that necessary modules are
    imported at context entering. All temporary imports and dependencies will be
    removed at context exit.
    """

    def __init__(self, obj_str):

        self._obj_str = obj_str
        self._object = None
        self._startup_modules = dict(sys.modules)

    def __enter__(self, *args):
        """
        Returns:
            (self) : necessary to enabled "with temporary_import() as" syntax

        Make temp. necessary imports.
        """

        self._object = self._get_object()
        return self

    def __exit__(self, *args):
        """ Remove temporary imports no matter what. """

        to_be_removed = []

        # get keys that were not in sys.modules at startup
        for x in sys.modules:
            if x not in self._startup_modules:
                to_be_removed.append(x)

        # remove "surplus" keys
        for x in to_be_removed or ():
            del sys.modules[x]

    @skipped_from_list
    def get_dir(self, silent=False):
        """
        Args:
            silent (bool, optional) : if True, AttributeError will be silenced

        Returns:
            (list[str] or None)

        Raises:
            (AttributeError) : if no object could be retrieve from the input string

        Get all names from self._object namespace.
        """

        if self._object is None:
            if silent:
                return None
            raise AttributeError('No module named \'{}\' found.'.format(self._obj_str))

        return dir(self._object)

    def as_string(self):
        return self._obj_str

    def object(self):
        """
        Returns:
            (object or None)

        Get the object from the context's input string. If no object could be
        retrieved, returns None.
        """

        return self._object

    @skipped_from_list
    def get_source_file(self, silent=False):
        """
        Args:
            silent (bool, optional) : if True, AttributeError will be silenced

        Returns:
            (str or None) : absolute path

        Raises:
            (AttributeError) : if no object could be retrieve from the input string

        Get self._object source file's path.
        """

        if self._object is None:
            if silent:
                return None
            raise AttributeError('No module named \'{}\' found.'.format(self._obj_str))

        if not hasattr(self._object, '__file__'):
            if silent:
                return None
            raise AttributeError('Could not get \'{}\' source file.'.format(self._obj_str))

        return self._object.__file__

    @skipped_from_list
    def _import_object(self, obj_str):
        """
        Args:
            obj_str (str)

        Returns:
            (object or None)

        Import object from <obj_str> et return it. If object could not be imported,
        returns None.
        """

        from_list = []
        if '.' in obj_str:
            from_list = ['.'.join(obj_str.split('.')[:-1])]

        try:
            obj = __import__(obj_str, locals(), globals(), from_list)
            return obj

        except:
            return None

    @skipped_from_list
    def _get_object(self):
        """
        Args:
            obj_str (str) : module, method or any object absolute name

        Returns:
            (object or None)

        Get object from <obj_str> if exists (import necessary modules).
        """

        if self._obj_str in sys.modules:
            return sys.modules[self._obj_str]

        roots = []
        obj = None

        for x in self._obj_str.split('.'):

            import_str = '{}.{}'.format('.'.join(roots), x) if roots else x
            module = self._import_object(import_str)

            # x is not an importable object anymore, use getattr() for now
            if not module:
                if not obj:
                    return None

                obj = getattr(obj, x, None)

            else:
                obj = module
                roots.append(x)

        return obj


def getdir(obj_str, silent=False):
    """
    Args:
        obj_str (str)
        silent (bool, optional)

    Get dir() list from <obj_str> using temporary_import() context manager.
    """

    with temporary_import(obj_str) as tmp_import:
        return tmp_import.get_dir(silent=silent)

def getfile(obj_str, silent=False):
    """
    Args:
        obj_str (str)
        silent (bool, optional)

    Get source file's path from <obj_str> using temporary_import() context manager.
    """

    with temporary_import(obj_str) as tmp_import:
        return tmp_import.get_source_file(silent=silent)
