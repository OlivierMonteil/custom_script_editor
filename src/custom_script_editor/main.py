"""
Main script for customizing Maya's script editor hightlights and hotkeys.
Installs eventFilter on Maya Window to install all features each time the Script
Editor is opened.
"""


import traceback

from PySide2 import QtWidgets, QtCore, QtGui

from maya import cmds
from maya import mel
import maya.OpenMayaUI as OMUI

import shiboken2 as shiboken

from custom_script_editor import utils
from custom_script_editor import constants as kk
from custom_script_editor.prefs import prefs_editor
from custom_script_editor import menu as tools_menu

from custom_script_editor.prefs.data import PreferencesManager
from custom_script_editor.managers import BlocksCollapseManager
from custom_script_editor.managers import MultiCursorManager
from custom_script_editor.managers import HighlightManager
from custom_script_editor.managers import SnippetsManager
from custom_script_editor.managers import KeysManager
from custom_script_editor.managers import FilterManager


class ScriptEditorDetector(QtCore.QObject):

    """
    Custom eventFilter installed on Maya's main window that will set all
    features on Script Editor as it is opened (evalDeferred).
    """

    def eventFilter(self, obj, event):
        """ (only effective QtCore.QEvent.ChildAdded) """

        # (no need to run set_customize_on_tab_change and customize_script_editor
        # if the Script Editor is already opened)
        if not utils.is_editor_opened('Script Editor'):
            if event.type() == QtCore.QEvent.ChildAdded:
                if event.child().isWidgetType():
                    # defer set_customize_on_tab_change and customize_script_editor to make sure
                    # Script Editor's window is fully created first
                    cmds.evalDeferred(set_customize_on_tab_change, lowestPriority=True)
                    cmds.evalDeferred(customize_script_editor, lowestPriority=True)

        if not utils.is_editor_opened('Expression Editor'):
            if event.type() == QtCore.QEvent.ChildAdded:
                if event.child().isWidgetType():
                    cmds.evalDeferred(customize_expression_editor, lowestPriority=True)

        return False


def set_customize_on_tab_change():
    """
    Connect ScriptEditor > QTabWidget.currentChanged signal to customize_script_editor().
    Used to customize new created tabs, as this signal is called after each tab
    creation.
    """

    tabsLay = utils.get_scripts_tab_lay()
    if not tabsLay:
        return

    ptr = OMUI.MQtUtil.findControl(tabsLay)
    qtLay = shiboken.wrapInstance(long(ptr), QtWidgets.QWidget)

    for child in qtLay.children():
        if isinstance(child, QtWidgets.QTabWidget):
            child.currentChanged.connect(customize_script_editor)
            break

def set_popup_menus_customizable():
    """
    Edit all Script Editor popupMenu's -postMenuCommand so customize_menu()
    will be called once the menu is created.
    """

    script_editor_popup_menus = utils.get_script_editor_popup_menus()

    for popup_menu in script_editor_popup_menus:
        if not cmds.popupMenu(popup_menu, q=True, exists=True):
            continue

        # lambda function seems to fail, so we will set postMenuCommand as a string
        cmd  = 'from custom_script_editor.main import customize_menu;'
        cmd += 'customize_menu("{}")'.format(popup_menu)

        cmds.popupMenu(popup_menu, e=True, postMenuCommand=cmd)

def set_console_word_wrap(enabled):
    """
    Args:
        enabled (bool)

    Set Script Editor's console line-wrap mode on <enabled> state.
    """

    console_field = utils.get_console_text_edit()
    if not console_field:
        return

    if enabled:
        console_field.setLineWrapMode(console_field.WidgetWidth)
    else:
        console_field.setLineWrapMode(console_field.NoWrap)

def customize_menu(menu):
    """
    Args:
        menu (str)

    Customize popupMenu on -postMenuCommand.
    """

    # run Maya's origin command (this command may remove custom menus that were
    # previously added).
    mel.eval("verifyCommandPopupMenus;")

    # add "Custom Menu" menu if not already in
    if not kk.CUSTOM_MENU_NAME in cmds.popupMenu(menu, q=True, itemArray=True):
        cmds.menuItem(
            kk.CUSTOM_MENU_NAME,
            p=menu,
            subMenu=True,
            radialPosition="S",
            label=kk.CUSTOM_MENU_LABEL
        )

    # make sure menuItem existence checks are using a long name
    custom_menu = '{}|{}'.format(menu, kk.CUSTOM_MENU_NAME)

    # script tabs only
    if 'cmdScrollFieldExecuter' in menu:
        # get tab language's default state
        language = utils.get_popup_menu_language(menu)
        prefs = utils.get_child_instance(PreferencesManager)
        state = prefs.get_by_names(language, 'snippets')

        # add "Snippets" box if not already in
        longname = '{}|{}'.format(custom_menu, kk.SNIPPETS_BOX_NAME)
        if not cmds.menuItem(longname, q=True, exists=True):
            cmds.menuItem(
                kk.SNIPPETS_BOX_NAME,
                p=custom_menu,
                radialPosition="S",
                label=kk.SNIPPETS_BOX_LABEL,
                checkBox=state
            )

    # console only
    if 'cmdScrollFieldReporter' in menu:
        # add "Word Wrap" box if not already in
        longname = '{}|{}'.format(custom_menu, kk.WORD_WRAP_BOX_NAME)
        if not cmds.menuItem(longname, q=True, exists=True):
            cmds.menuItem(
                kk.WORD_WRAP_BOX_NAME,
                p=custom_menu,
                radialPosition="S",
                label=kk.WORD_WRAP_BOX_LABEL,
                checkBox=False,
                command=set_console_word_wrap
            )

    # add "Custom Tools" menu if not already in
    longname = '{}|{}'.format(custom_menu, kk.CUSTOM_TOOLS_MENU_NAME)
    if not cmds.menuItem(longname, q=True, exists=True):
        cmds.menuItem(
        kk.CUSTOM_TOOLS_MENU_NAME,
        p=custom_menu,
        radialPosition="W",
        label=kk.CUSTOM_TOOLS_MENU_LABEL,
        command=run_script_tools_menu
    )

    # add "Edit palettes..." menu if not already in
    longname = '{}|{}'.format(custom_menu, kk.PREFERENCES_MENU_NAME)
    if not cmds.menuItem(longname, q=True, exists=True):
        cmds.menuItem(
            kk.PREFERENCES_MENU_NAME,
            p=custom_menu,
            radialPosition="E",
            label=kk.PREFERENCES_MENU_LABEL,
            command=run_prefs_editor
        )

def run_script_tools_menu(menu):
    """ Run "Script Tools" menu. """

    pos = QtGui.QCursor().pos()
    tools_menu.run(pos)

def run_prefs_editor(menu):
    """ Run "Edit palette..." menu. """

    pos = QtGui.QCursor().pos()
    prefs_editor.run(pos)

def edit_line_numbers_cmd():
    """
    Args:
        script_editor_is_opened (bool)

    Overwrite Maya's "Show line numbers" command, as this tool has its own
    line-numbers widget implementation.
    """

    if cmds.optionVar(q='commandExecuterShowLineNumbers'):
        mel.eval('toggleCommandExecuterShowLineNumbers()')
        # edit Maya's variables so Maya line-numbers won't be shown
        cmds.optionVar(iv=('commandExecuterShowLineNumbers', False))

    mel_proc = '''
    global proc toggleCommandExecuterShowLineNumbers(){
        global string $showLineNumbersMenuItemSuffix;
        string $menus[] = `lsUI -menuItems`;

        for ($menu in $menus) {
            if (`match $showLineNumbersMenuItemSuffix $menu` != ""){
                // set "Show line numbers" menu item un-checked and disabled
                menuItem -e -enable false $menu;
                menuItem -e -checkBox false $menu;
            }
        }
        // disable also script editor toolbar's icon button
        iconTextButton -e -enable false -image "showLineNumbersOff.png" showLineNumbersButton;

        print("// Info : \\"Show line numbers\\" is disabled by Custom Script Editor.\\n");
    }
    '''

    mel.eval(mel_proc)

def edit_line_numbers_controls():
    """
    Disable Maya's "Show line numbers" controls, as this tool has its own
    line-numbers widget implementation.
    """

    # disable script editor toolbar's icon button
    try:
        cmds.iconTextButton(
            'showLineNumbersButton', e=True, enable=False, image="showLineNumbersOff.png"
        )
    except:
        try:
            cmds.iconTextCheckBox(
            'showLineNumbersButton', e=True, enable=False, v=False
            )
        except:
            pass

def customize_expression_editor(*args):

    expr_editor = utils.get_expression_txt_edit()
    if not expr_editor:
        return

    highlight_manager = utils.get_manager(HighlightManager)
    keys_manager = utils.get_manager(KeysManager)
    multi_cursor_manager = utils.get_manager(MultiCursorManager)

    utils.get_overlay(expr_editor, False)

    if expr_editor and highlight_manager.is_needed(expr_editor, 'expressions'):
        highlight = highlight_manager.apply(expr_editor, 'expressions')
        highlight.rehighlight()

    if keys_manager.is_needed(expr_editor, 'expressions'):
        keys_manager.apply(expr_editor, 'expressions')

    if multi_cursor_manager.is_needed(expr_editor, 'expressions'):
        multi_cursor_manager.apply(expr_editor)


def customize_script_editor(*args):
    """
    Iterate every tab from Script Editor and apply allfeatures if necessary.
    """

    edit_line_numbers_controls()

    blocks_collapse_manager = utils.get_manager(BlocksCollapseManager)
    highlight_manager = utils.get_manager(HighlightManager)
    filter_manager = utils.get_manager(FilterManager)
    snippets_manager = utils.get_manager(SnippetsManager)
    keys_manager = utils.get_manager(KeysManager)
    multi_cursor_manager = utils.get_manager(MultiCursorManager)

                #############################################
                #                Edit Console               #
                #############################################

    console_field = utils.get_console_text_edit()

    if console_field:
        if console_field and highlight_manager.is_needed(console_field, 'console'):
            highlight = highlight_manager.apply(console_field, 'console')
            highlight.rehighlight()

        if console_field and filter_manager.is_needed(console_field):
            filter_manager.apply(console_field)

                #############################################
                #                Script Tabs                #
                #############################################

    se_tab_lay = utils.get_scripts_tab_lay()
    if not se_tab_lay:
        return

    # get all ScriptEditor tabs and tab-labels
    labels = cmds.tabLayout(se_tab_lay, q=True, tabLabel=True)
    tabs = cmds.tabLayout(se_tab_lay, q=True, childArray=True)
    selected_tab = cmds.tabLayout(se_tab_lay, q=True, selectTabIndex=True)

    for i, form_lay in enumerate(tabs) or ():
        # only install highlight on selected tab
        if not i == selected_tab -1:
            continue

        # get tab language
        tab_type = 'python' if utils.is_valid_tab_name(labels[i], exlude_mel=True) else \
                   'mel'

        ptr = OMUI.MQtUtil.findControl(form_lay)
        widget = shiboken.wrapInstance(long(ptr), QtWidgets.QWidget)
        text_edits = utils.get_text_edits(widget)

        for txt_edit in text_edits or ():
            if not 'cmdScrollFieldExecuter' in txt_edit.objectName():
                continue

            try:
                # create overlay if not already
                utils.get_overlay(txt_edit)

                # install highlight if not already set
                if highlight_manager.is_needed(txt_edit, tab_type):
                    highlight_manager.apply(txt_edit, tab_type, padding=True)

                # install KeysHandler filterEvent on QTextEdit if not already installed
                if keys_manager.is_needed(txt_edit, tab_type):
                    keys_manager.apply(txt_edit, tab_type)

                # install SnippetsHandler filterEvent on QTextEdit if not already installed
                if snippets_manager.is_needed(txt_edit, tab_type):
                    snippets_manager.apply(txt_edit, form_lay, tab_type)

                # install MultiCursorHandler filterEvent on QTextEdit if not already installed
                if multi_cursor_manager.is_needed(txt_edit, tab_type):
                    multi_cursor_manager.apply(txt_edit)

                if blocks_collapse_manager.is_needed(txt_edit, tab_type):
                    blocks_collapse_manager.apply(txt_edit)

            except Exception as e:
                print (kk.ERROR_MESSAGE.format(e))
                traceback.print_exc()

                #############################################
                #                Quick Help                 #
                #############################################

    quick_help = utils.get_quick_help_widget()
    if quick_help and highlight_manager.is_needed(quick_help, 'quick-help'):
        highlight_manager.apply(quick_help, 'quick-help')

                #############################################
                #                Popup-menus                #
                #############################################

    set_popup_menus_customizable()


@utils.catch_error
def run():
    """
    (called by maya's userSetup.mel/py)

    Install event filter on Maya's main window to automate Script Editor
    customization.
    """

    # skip if maya is in batch mode
    if cmds.about(batch=True):
        return

    edit_line_numbers_cmd()

    maya_ui_qt = utils.get_maya_window()

    # install ScriptEditorDetector event filter on Maya window if not already
    if utils.is_child_class_needed(maya_ui_qt, ScriptEditorDetector):
        ui_filter = ScriptEditorDetector(parent=maya_ui_qt)
        maya_ui_qt.installEventFilter(ui_filter)

    # customize Script Editor if already opened and connect
    # ScriptEditor > QTabWidget.currentChanged signal to customize_script_editor.
    # This will allow to customize all new created tabs, because this signal is
    # called after each tab creation when the Script Editor automatically focuses
    # on the new tab.

    if utils.is_editor_opened('Script Editor'):
        cmds.evalDeferred(customize_script_editor, lp=True)
        cmds.evalDeferred(set_customize_on_tab_change, lp=True)
    if utils.is_editor_opened('Expression Editor'):
        cmds.evalDeferred(customize_expression_editor, lp=True)

    print (kk.SUCCESS_MESSAGE.format('activated.'))
