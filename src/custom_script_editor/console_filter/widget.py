"""
The widget parented to the Script Editor's console.
"""

import os
from PySide2 import QtWidgets, QtGui

from custom_script_editor import constants as kk


class ToggleButton(QtWidgets.QPushButton):

    _size = 28
    _padding = 5
    _tooltips = [
        'click to apply filter',
        'click to clear filter',
    ]

    def __init__(self, txt_edit, splitter, script_editor, state=False):
        super(ToggleButton, self).__init__('', txt_edit)

        self._state = state
        self._txt_edit = txt_edit
        self._splitter = splitter

        self._on_map = QtGui.QPixmap(os.path.join(kk.ICONS_ROOT, 'filter.png'))
        self._off_map = QtGui.QPixmap(os.path.join(kk.ICONS_ROOT, 'no_filter.png'))

        self.setToolTip(self._tooltips[self._state])
        self.setFixedSize(self._size, self._size)

        self.clicked.connect(self._toggle)
        script_editor.installEventFilter(self)

        self._align_with_parent()

    def _toggle(self):
        """
        Toggle button's pixmap and tooltip.
        """

        self._state = not self._state
        self.setToolTip(self._tooltips[self._state])
        self.repaint()

    def eventFilter(self, obj, event):
        """
        Qt re-implementation, maintain the button in the right corner of the
        console on Script Editor's resizeEvent.
        """

        if event.type() == event.Resize:
            self._align_with_parent()
        return False

    def _align_with_parent(self):
        self.move(
            self._splitter.width() -self._txt_edit.verticalScrollBar().width() -self._size -self._padding,
            self._padding
        )

    def paintEvent(self, event):
        """ Qt re-implementation. """

        painter = QtGui.QPainter(self)
        painter.setRenderHint(painter.Antialiasing, True)
        painter.setRenderHint(painter.HighQualityAntialiasing, True)
        painter.setRenderHint(painter.SmoothPixmapTransform, True)

        pixmap = self._on_map if self._state else self._off_map

        painter.drawPixmap(0, 0, pixmap)
        painter.end()
