"""
This package is made for filtering Scipt Editor's console log content using .json
rule files.
"""

# shorten absolute imports
from custom_script_editor.console_filter.handler import FilterHandler
from custom_script_editor.console_filter.data import FilterData
