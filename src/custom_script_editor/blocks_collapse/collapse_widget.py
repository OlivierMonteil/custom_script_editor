"""
Text blocks expand/collapse widgets. The update of the CollapseButtons is handled
into the CollapseWidget class.
"""


import re

from PySide2 import QtWidgets, QtCore, QtGui

from custom_script_editor.blocks_collapse import TextEditCollapser
from custom_script_editor import constants as kk
from custom_script_editor import utils


class CollapseWidget(QtWidgets.QWidget, TextEditCollapser):
    """
    Left-padding-area widget which will manage the CollapseButtons.
    """

    blocks_visibility_changed = QtCore.Signal(int, int, bool)
    offsetX = 2

    def __init__(self, txt_edit):
        QtWidgets.QWidget.__init__(self, parent=txt_edit)

        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self._txt_edit = txt_edit
        self._buttons = []
        self._too_small_to_be_shown = False

        self.resize(kk.LEFT_PADDING, kk.INF_HEIGHT)

        self.update_all()

    def update_all(self):
        """
        Update button's positions (updating self position from scrollbar's value)
        and buttons (adding/removing missing/un-neccary buttons).
        """

        self.update_position()      # update self.position
        self._update_buttons()              # update CollapseButtons

    def _update_buttons(self):
        """
        Add/remove necessary/un-necessary CollapseButtons and update all buttons
        position from corresponding block's positions.

        The commented lines where for an attempt to perform this update on the
        QTextEdit's visible area only. As this process was not working well, it
        would need to be re-examinated.
        """

        if self._too_small_to_be_shown:
            for button in self._buttons or ():
                button.setParent(None)
                button.deleteLater()

            self._buttons = []
            return

        block_count = self._txt_edit.document().blockCount()

        # these two lists will be used to know which buttons are to be removed
        kept_buttons = []
        old_buttons = list(self._buttons)

        for i in range(0, block_count) or ():
            block = self._txt_edit.document().findBlockByNumber(i)

            # do not show button on already collapsed lines
            if not block.isVisible():
                continue

            text = block.text().strip()
            # block matches a pattern
            if any(text.startswith(x) for x in kk.COLLAPSIBLE_PATTERNS):
                found = self._get_button(block)
                # if this block has already an associated button, just update
                # its position
                if found:
                    kept_buttons.append(found)
                    found.update_position()
                    continue

                # else, create a new button
                new_button = self._add_button(block)
                self._buttons.append(new_button)

        # remove all unused buttons
        for button in reversed(old_buttons) or ():
            if button not in kept_buttons:
                self._buttons.remove(button)
                button.setParent(None)
                button.deleteLater()

        # force CollapseWidget to update entirely
        self.update(self.geometry())

    def update_position(self, *args):
        """
        Match CollapseWidget's position with the scrolling area (using the
        vertical scrollbar value).
        """

        self.move(
            QtCore.QPoint(
                self.offsetX,
                -self._txt_edit.verticalScrollBar().value()
            )
        )

    def _add_button(self, block):
        """
        Args:
            block (QtGui.QTextBlock)

        Create new CollapseButton for <block>. Set its collapsed state from the
        next block's visibility state.
        """

        collapsed = True if not block.next().isVisible() else False

        button = CollapseButton(block, self, collapsed)
        # update CollapseWidget on block collapse/expand
        button.state_changed.connect(self.blocks_visibility_changed.emit)
        button.collapse_all_asked.connect(self._collapse_all_buttons)

        button.show()
        button.update_position()

        return button

    def _collapse_all_buttons(self, state):
        for button in self._buttons:
            if button.is_collapsed() != state:
                button.toggle_visibility()

    def _get_button(self, block):
        """
        Args:
            block (QtGui.QTextBlock)

        Returns:
            (CollapseButton or None)

        Get the existing CollapseButton that is associated to the <block>.
        """

        for button in self._buttons or ():
            if button.block == block:
                return button

    def update_zoom(self, *args):
        """
        Hide buttons if text is too small, else update their positions.
        """

        if CollapseButton.radius >= self._get_line_height():
            self._too_small_to_be_shown = True
        else:
            self._too_small_to_be_shown = False

        self._update_buttons()

    def _get_line_height(self):
        """
        Returns:
            (int)

        Get lines height value.
        """

        metrics = QtGui.QFontMetrics(self._txt_edit.font())
        return metrics.height() +metrics.lineSpacing()/2


class CollapseButton(QtWidgets.QPushButton):
    """
    QtGui.QTextBlock expand/collapse button.
    """

    state_changed = QtCore.Signal(int, int, bool)
    collapse_all_asked = QtCore.Signal(bool)

    colors = [
        QtGui.QColor(207, 228, 255, 100),
        QtGui.QColor(89, 166, 61)
    ]

    radius = 10

    def __init__(self, block, collapse_widget, collapsed=False):
        """
        Args:
            block (QtGui.QTextBlock)
            collapse_widget (CollapseWidget)
            collapsed (bool, optional)
        """

        super(CollapseButton, self).__init__(collapse_widget)

        self.block = block
        self._collapse_widget = collapse_widget
        self._txt_edit = collapse_widget._txt_edit
        self._is_collapsed = collapsed

        self.setFixedSize(self.radius+2, self.radius+2)


    def mousePressEvent(self, event):
        """
        Qt re-implementation, collapse/Expand all on Ctrl/Shift modifiers. Else,
        simply toogle the clicked one.
        """

        collapse_all = None

        if event.modifiers() == QtCore.Qt.ControlModifier:
            collapse_all = True
        if event.modifiers() == QtCore.Qt.ShiftModifier:
            collapse_all = False

        if collapse_all is None:
            self.toggle_visibility()
        else:
            self.collapse_all_asked.emit(collapse_all)

    def toggle_visibility(self):
        """
        Toggle text block visibility (show/hide all  QtGui.QTextBlocks under
        self.block that matches grater indentation-level).

        For line breaks, a pending-block system will be used to manage their
        state without taking their indentation-level in account.
        """

        block = self.block
        start_indent_level = utils.get_indent_level(block)

        start = None
        state = None
        length = 0
        start_block = block.blockNumber() +1
        end_block = block.blockNumber() +1

        while True:
            block = block.next()
            # get the visibility state to apply from the first next block
            if state is None:
                state = not block.isVisible()

            # the end of the document is reached
            if block.blockNumber() == -1:
                break

            indent_level = utils.get_indent_level(block, ignore_white_lines=True)

            # increment counters
            if start is None:
                start = block.position()
            length += block.length()

            # (indent_level is None on line breaks)
            if indent_level is None or indent_level > start_indent_level:
                continue

            break

        # MEL proc closure
        if block.text().startswith('}'):
            end_block = block.blockNumber()
        # do not collapse ending line breaks
        else:
            while re.match('^\s*$', block.previous().text()):
                block = block.previous()
            end_block = block.blockNumber()

        # toggle button's aspect
        self._is_collapsed = not self._is_collapsed
        self.repaint()

        self.state_changed.emit(start_block, end_block, state)

    def update_position(self):
        """
        Maintain CollapseButton aligned with its associated QtGui.QTextBlock.
        """

        block_lay = self.block.layout()
        rect = block_lay.boundingRect()
        pos = block_lay.position().toPoint()

        self.move(4, pos.y() -1 +(rect.height() -self.radius)/2)

    def is_collapsed(self):
        """ Returns: (bool) """
        return self._is_collapsed

    def paintEvent(self, event):
        """
        Qt re-implementation, paints the CollapseButton itself, with a simple
        `-` / `+` paint, depending on collapsed state.
        """

        painter = QtGui.QPainter(self)
        painter.setBrush(QtCore.Qt.transparent)
        painter.setPen(self.colors[self._is_collapsed])

        painter.drawRect(QtCore.QRect(0, 0, self.radius, self.radius))
        # draw horizontal line
        painter.drawLine(3, self.radius/2, self.radius -3, self.radius/2)

        if self.is_collapsed():
            # draw vertical line
            painter.drawLine(self.radius/2, 3, self.radius/2, self.radius -3)
