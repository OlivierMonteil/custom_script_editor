"""
Text blocks expand/collapse system, by adding a CollapseWidget on QTextEdits and
Collapse Buttons in association to class/def declaration blocks.

The custom_script_editor.managers.BlocksCollapseManager class is used to add this
system to the Script Editor tabs QTextEdit instances.

Also, the line-numbers display is re-implemented so it will match blocks collapse.
"""

# shorten absolute imports
from custom_script_editor.blocks_collapse.common import TextEditCollapser
from custom_script_editor.blocks_collapse.line_numbers import LineNumbersWidget
from custom_script_editor.blocks_collapse.collapse_widget import CollapseWidget
from custom_script_editor.blocks_collapse.handler import CollapseHandler
