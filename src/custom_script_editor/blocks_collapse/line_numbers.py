"""
Line-numbers display widget (replaces the Maya one, so block collapse will also
be applied on line-numbers).
"""

from PySide2 import QtWidgets, QtCore

from custom_script_editor import constants as kk
from custom_script_editor.blocks_collapse import TextEditCollapser


class LineNumbersWidget(QtWidgets.QTextEdit, TextEditCollapser):
    """
    Vertical QTextEdit for line numbers display. Parented to some Script Editor
    tab's QTextEdit, it wil be connected to it by the CollapseHandler so line
    numbers will be aligned with the corresponding blocks and match their
    collapse/expand state.
    """

    blocks_visibility_changed = QtCore.Signal(int, int, bool)

    def __init__(self, txt_edit):
        QtWidgets.QTextEdit.__init__(
            self,
            parent=txt_edit,
            readOnly=True,
            horizontalScrollBarPolicy=QtCore.Qt.ScrollBarAlwaysOff,
            verticalScrollBarPolicy=QtCore.Qt.ScrollBarAlwaysOff,
            lineWrapMode=QtWidgets.QTextEdit.NoWrap
        )

        self._txt_edit = self
        self._parent_txt_edit = txt_edit

        self.setAlignment(QtCore.Qt.AlignRight)
        self.setAttribute(QtCore.Qt.WA_TransparentForMouseEvents)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self.document().setUseDesignMetrics(True)

        # set font on both QTextEdits at startup, so we are sure that both sizes
        # will match
        font = self._parent_txt_edit.currentFont()
        font.setFamily(self._parent_txt_edit.font().family())
        font.setPixelSize(12)
        font.setHintingPreference(font.PreferVerticalHinting)
        self._parent_txt_edit.setFont(font)
        self.setFont(font)

        # set widget's size (until a better system is applied, we will set its
        # height at an infinite value)
        self.resize(kk.LEFT_PADDING, kk.INF_HEIGHT)
        self.setFixedWidth(kk.LEFT_PADDING)
        self.setStyleSheet(
        '''
            QTextEdit {
                background-color: rgba(207, 228, 255, 20);
                color : rgba(255, 255, 255, 100);
                border: none;
            }
        '''
        )
        # make sure self position matches parent QtextEdit
        self.move(0, 0)

        # set line numbers at startup
        self._add_blocks(self._parent_txt_edit.document().blockCount())

    def update_scrollbar_from_parent(self, *args):
        """
        Set self vertical scrollbar values to match parent QtextEdit's.
        """

        line_bar = self.verticalScrollBar()
        parent_bar = self._parent_txt_edit.verticalScrollBar()

        line_bar.setRange(parent_bar.minimum(), parent_bar.maximum())
        line_bar.setValue(parent_bar.value())

    def update_font(self):
        """
        Into Maya Script Editor, text zoomIn/Out is handled by editing the
        pixelSize(), so we need to set self pixelSize() to match tab text's.
        """

        px_size = self._parent_txt_edit.currentFont().pixelSize()

        font = self.font()
        font.setPixelSize(px_size)
        self.setFont(font)

    def update_block_count(self, count):
        """
        Args:
            count (int) : the number of blocks into parent QTextEdit

        Add/remove line-number blocks to match parent QTextEdit's block count.
        """

        delta = count -self.document().blockCount()

        if delta > 0:
            self._add_blocks(delta)
        elif delta == 0:
            self._add_blocks(1)
        else:
            self._remove_blocks(delta)

    def _add_blocks(self, count):
        """
        Args:
            count (int)

        Add <count> blocks into self with line numbers set.
        """

        last_block = self.document().lastBlock()
        last_block_number = last_block.blockNumber()
        cursor = self.textCursor()
        cursor.movePosition(cursor.End, cursor.MoveAnchor)

        if last_block.text():
            cursor.insertText('\n')
            last_block_number += 1

        for i in range(count):
            cursor.insertText(str(last_block_number +i +1))

            if i < count -1:
                cursor.insertText('\n')

    def _remove_blocks(self, count):
        """
        Args:
            count (int)

        Add <count> blocks from self.
        """

        cursor = self.textCursor()
        cursor.movePosition(cursor.End, cursor.MoveAnchor)

        for _ in range (-count):
            cursor.select(cursor.LineUnderCursor)       # select block's text
            cursor.removeSelectedText()                 # clear block's text
            cursor.deleteChar()                         # clear the block itself
            cursor.movePosition(cursor.Up, cursor.MoveAnchor)
