"""
Text blocks expand/collapse widgets. The update of the CollapseButtons is handled
into the CollapseWidget class.
"""


from PySide2 import QtCore


class TextEditCollapser(QtCore.QObject):
    """
    Blocks collapse/expand handling class.
    """

    def set_blocks_visibility(self, start_block, end_block, state):
        """
        Args:
            start_block (int)
            end_block (int)
            state (bool)

        Force blocks from start/end input-range's visibility to <state>.
        """

        start = None
        length = 0

        for i in range(start_block, end_block):
            block = self._txt_edit.document().findBlockByNumber(i)
            block.setVisible(state)

            if start is None:
                start = block.position()
            length += block.length()

        self.update_document(start, length +1)

    def update_document(self, start, length):
        """
        Args:
            start (int) : start position in document
            length (int)

        Trigger QTextEdit's update through QTextDocument.markContentsDirty slot.
        It seems to be the only way to update properly the QTextEdit after editing
        some QTextBlocks visibility.
        """

        start = 0 if start is None else start
        self._txt_edit.document().markContentsDirty(start, length)

    def set_slider_position(self, position):
        scroll_bar = self._txt_edit.verticalScrollBar()
        scroll_bar.setSliderPosition(position)
