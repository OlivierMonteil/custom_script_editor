"""
Text blocks expand/collapse widgets. The update of the CollapseButtons is handled
into the CollapseWidget class.
"""


from PySide2 import QtCore, QtGui

from custom_script_editor.blocks_collapse import LineNumbersWidget
from custom_script_editor.blocks_collapse import CollapseWidget


class CollapseHandler(QtCore.QObject):
    """
    Left-padding-area widget which will manage the CollapseButtons.
    """

    def __init__(self, parent=None, collapsible=True):
        super(CollapseHandler, self).__init__(parent=parent)

        self._txt_edit = parent
        self._collapsible = collapsible
        self._collapse_widget = None
        self._lines_widget = None

    def install(self):
        """
        Apply line-numbers and blocks-collapse widgets last one only if required).
        """

                        ###################################
                        #         Create widgets          #
                        ###################################

        if self._collapsible:
            self._collapse_widget = CollapseWidget(self._txt_edit)

        self._lines_widget = LineNumbersWidget(self._txt_edit)

                        ###################################
                        #         Connect Signals         #
                        ###################################

        # update on QTextEdit zoom in/out
        txt_bar = self._txt_edit.verticalScrollBar()
        text_layout = self._txt_edit.findChild(QtGui.QAbstractTextDocumentLayout)

        txt_bar.valueChanged.connect(self._update_from_txt_edit_scrollbar)
        txt_bar.rangeChanged.connect(self._update_from_txt_edit_scrollbar)
        txt_bar.sliderMoved.connect(self._update_from_txt_edit_scrollbar)
        # zoom in/out detection
        text_layout.documentSizeChanged.connect(self._update_from_txt_edit_zoom)

        # blocks-collapse widget's specific connections
        if not self._collapse_widget is None:
            # connect expand/collapse to line-numbers widget expand/collapse
            self._collapse_widget.blocks_visibility_changed.connect(
                self.set_blocks_visibility_on_both
            )
            self._txt_edit.textChanged.connect(self._collapse_widget.update_all)

        # line-numbers widget's specific connection
        self._txt_edit.document().blockCountChanged.connect(
            self._update_on_block_count_changed
        )

                        ###################################
                        #         Startup update          #
                        ###################################

        # force line-numbers scrollbar's update from self._txt_edit's
        self._lines_widget.update_scrollbar_from_parent()

        self._collapse_widget.show()
        self._lines_widget.show()

    def _update_from_txt_edit_zoom(self, *args):
        """
        Update line-numbers and blocks-collapse widgets on parent QTextEdit
        zoomIn/Out (Ctrl+Wheel).
        """

        if not self._collapse_widget is None:
            self._collapse_widget.update_zoom()

        self._lines_widget.update_font()
        # to avoid font update from occuring after the update of the scrollbar
        # (and scrolling the line numbers to the top), we will defer a bit this
        # update with a short timer
        QtCore.QTimer.singleShot(30, self._lines_widget.update_scrollbar_from_parent)

    def _update_on_block_count_changed(self, count):
        """
        Args:
            count (int) : the number of blocks in the script QTextEdit

        Update both line-numbers count and visibility.
        """

        self._lines_widget.update_block_count(count)
        self._update_line_numbers_visiblity()

    def _update_line_numbers_visiblity(self):
        """
        Update all line-numbers blocks visibility (necessary if some lines were
        inserted before a collapsed group).
        """

        # we need to catch the first and last blocks updated to refresh them properly
        first = None
        last = None

        for i in range(self._txt_edit.document().blockCount()):
            block = self._get_block_by_number(self._txt_edit, i)
            line_num_block = self._get_block_by_number(self._lines_widget, i)

            block_vis = block.isVisible()

            if line_num_block.isVisible() != block_vis:
                line_num_block.setVisible(block_vis)

                if first is None:
                    first = i
                last = i

        # refresh the edited blocks if some
        if first is not None:
            start = self._get_block_by_number(self._lines_widget, first).position()
            length = 0

            for i in range(first, last):
                length += self._get_block_by_number(self._lines_widget, i).length()

            self._lines_widget.update_document(start, length)

    def _get_block_by_number(self, txt_edit, i):
        return txt_edit.document().findBlockByNumber(i)

    def _update_from_txt_edit_scrollbar(self):
        """
        Update line-numbers and blocks-collapse widgets on parent QTextEdit's
        QScrollBar changes.
        """

        if not self._collapse_widget is None:
            self._collapse_widget.update_position()

        self._lines_widget.update_scrollbar_from_parent()

    def set_blocks_visibility_on_both(self, start_block, end_block, state):
        """
        Args:
            start_block (int)
            end_block (int)
            state (bool)

        Collapse/expand blocks from input range on both line-numbers and parent
        QTextEdit.
        """

        self._collapse_widget.set_blocks_visibility(start_block, end_block, state)
        self._lines_widget.set_blocks_visibility(start_block, end_block, state)

        self._update_from_txt_edit_scrollbar()
