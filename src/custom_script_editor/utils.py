"""
Some utility fonctions and classes.
"""

import re
import traceback
import cProfile
import pstats

import StringIO as io
import maya.OpenMayaUI as OMUI
from maya import cmds
from maya import mel

from PySide2 import QtWidgets, QtGui
import shiboken2 as shiboken

from custom_script_editor import constants as kk
from custom_script_editor.prefs import PreferencesManager
from custom_script_editor.overlay import OverlayWidget


                ###########################################
                #               Decorators                #
                ###########################################

def catch_error(func):
    """
    Non-blocking error-catcher decorator (formats error and prints traceback).
    """

    def wrap(*args, **kwargs):
        try:
            return func(*args, **kwargs)

        except Exception as e:
            print (kk.ERROR_MESSAGE.format(e))
            traceback.print_exc()

    wrap.__doc__ = func.__doc__
    return wrap

def dump_stats(func):
    """ Decorator for cProfile stats print. """

    def wrap(*args, **kwargs):
        try:
            with profiling():
                result = func(*args, **kwargs)
            return result
        except Exception as e:
            print (kk.ERROR_MESSAGE.format(e))
    return wrap

def widgets_counter(func):
    """ Decorator for top-level widgets count before/after function call. """

    def wrap(*args, **kwargs):
        try:
            with widgets_check():
                result = func(*args, **kwargs)
            return result
        except Exception as e:
            print (kk.ERROR_MESSAGE.format(e))
    return wrap


                ###########################################
                #            Context Managers             #
                ###########################################

class profiling(object):
    """
    Context manager for cProfile stats print.
    """

    def __init__(self):
        self._profiler = cProfile.Profile()

    def __enter__(self, *args):
        """ Enable cProfile. """
        self._profiler.enable()

    def __exit__(self, *args):
        """ Disable cProfile and print stats. """
        self._profiler.disable()

        s = io.StringIO()
        sort_by = 'cumulative'
        ps = pstats.Stats(self._profiler, stream=s).sort_stats(sort_by)
        ps.print_stats()

        print(s.getvalue())


class updates_disabled(object):

    def __init__(self, widget):
        self._widget = widget

    def __enter__(self, *args):
        self._widget.setUpdatesEnabled(False)

    def __exit__(self, *args):
        self._widget.setUpdatesEnabled(True)


class widgets_check(object):
    """
    Context manager for top-level widgets count before/after function call.
    Just an utility class to make sure all create widgets within this manager
    are well parented and destroyed on window close.
    """

    print_widgets = True

    def __init__(self):
        self._widgets_before = None
        self._widgets_after = None

    def __enter__(self, *args):
        """ Store top-level widgets count before function call. """
        self._widgets_before = QtWidgets.qApp.topLevelWidgets()

    def __exit__(self, *args):
        """ Get top-level widgets count after function call and print the result. """
        self.print_delta()
        return self

    def print_delta(self):

        if self._widgets_after is not None:
            self._widgets_before = self._widgets_after

        self._widgets_after = QtWidgets.qApp.topLevelWidgets()

        added_widgets = [x for x in self._widgets_after if not x in self._widgets_before]

        print ('# Added top-level widgets : {}'.format(len(added_widgets)))

        if self.print_widgets:
            for x in added_widgets or ():
                name = x.objectName() if hasattr(x, 'objectName') else ''
                title = x.windowTitle() if hasattr(x, 'windowTitle') else ''
                print ('  {}  |  {}  |  {}'.format(x, name, title))

        print ('# top-level widgets TOTAL count : {} top-level widgets.\n'.format(
                len(self._widgets_after)
            )
        )


                ###########################################
                #              Misc functions             #
                ###########################################


def get_maya_window():
    """
    Returns:
        (QtWidgets.QMainWindow)

    Get Maya's main window as wrapped instance (shiboken).
    """

    maya_ui = OMUI.MQtUtil.mainWindow()
    return shiboken.wrapInstance(long(maya_ui), QtWidgets.QMainWindow)

def get_script_editor():
    """
    Returns:
        (QtWidgets.QWidget)

    Gets the Script Editor window.
    """

    # following lines fail at Script Editor Opening, so we will check top level
    # widgets title instead :

    # script_editor = OMUI.MQtUtil.findWindow('scriptEditorPanel1Window')
    # if script_editor:
    #     return None
    #
    # return shiboken.wrapInstance(long(script_editor), QtWidgets.QWidget)

    return get_window_by_title('Script Editor')

def get_expression_txt_edit():
    """
    Returns:
        (QtWidgets.QTextEdit)
    """

    expression_editor =  get_window_by_title('Expression Editor')
    if not expression_editor:
        return
    return get_text_edits(expression_editor)[0]

def get_window_by_title(title):
    """
    Args:
        (str)

    Returns:
        (QtWidgets.QWidget)
    """

    app = QtWidgets.QApplication.instance()
    for widget in app.topLevelWidgets():
        if widget.windowTitle() == title:
            return widget

def is_editor_opened(title):
    """ Returns: (bool) """
    return True if get_window_by_title(title) else False

def get_script_editor_popup_menus():
    return mel.eval("$CSE_cmdPopupMenus = $gCommandPopupMenus;")

def get_scripts_editor_tab_layouts():
    """
    Returns:
        (str or None) : tabLayout

    Get Script Editor's tabLayout that is holding script tabs (not the "Quick Help" one).
    """

    # get scriptEditor panel if exists
    try:
        panels = cmds.lsUI(panels=True)
    except:
        return None

    script_editor = [p for p in panels if 'scriptEditor' in p]
    if not script_editor:
        return None

    # get all tablayouts that have scriptEditor as parent
    return [
        lay for lay in cmds.lsUI(type='tabLayout')
        if script_editor[0] in cmds.layout(lay, q=True, p=True)
    ]

def get_scripts_tab_lay():
    """
    Returns:
        (str or None) : tabLayout

    Get Script Editor's tabLayout that is holding script tabs (not the "Quick Help" one).
    """

    for lay in get_scripts_editor_tab_layouts() or ():
        tabs = cmds.tabLayout(lay, q=True, tabLabel=True)

        if all(is_valid_tab_name(x) for x in tabs):
            return lay

def get_quick_help_widget():
    """
    Returns:
        (str or None) : tabLayout

    Get Script Editor's tabLayout that is the "Quick Help".
    """

    for lay in get_scripts_editor_tab_layouts() or ():
        tabs = cmds.tabLayout(lay, q=True, tabLabel=True)

        if tabs and tabs[0] == 'Quick Help':
            form_lay = cmds.tabLayout(lay, q=True, childArray=True)[0]
            ptr = OMUI.MQtUtil.findControl(form_lay)
            widget = shiboken.wrapInstance(long(ptr), QtWidgets.QWidget)

            for child in widget.children():
                if isinstance(child, QtWidgets.QListWidget):
                    return child

def get_text_edits(widget):
    """
    Args:
        widget (QWidget)

    Recursively get all QTextEdit found into widget's children.
    """

    if not hasattr(widget, 'children'):
        return []

    found = []
    for child in widget.children() or ():
        if isinstance(child, QtGui.QTextDocument):
            found.append(widget.parent())

        found.extend(get_text_edits(child))

    return found

def get_console_text_edit():
    """
    Returns:
         (QTextEdit)

    Get console logs QTextEdit from Script Editor panel.
    """

    script_editor = get_script_editor()
    if not script_editor:
        return None

    txt_edits = get_text_edits(script_editor)

    for te in txt_edits:
        if 'cmdScrollFieldReporter' in te.objectName():
            return te

def get_popup_menu_language(menu):
    """
    Args:
        menu (str)

    Returns:
        (str) : 'python' or 'mel'

    Get <menu>'s associated tab language.
    """

    tokens = []

    for x in menu.split('|'):
        tokens.append(x)

        if 'tabLayout' in x:
            break

    tab_layout = '|'.join(tokens)
    idx = cmds.tabLayout(tab_layout, q=True, selectTabIndex=True)
    tab_label = cmds.tabLayout(tab_layout, q=True, tabLabel=True)[idx-1]

    return 'python' if is_valid_tab_name(tab_label, exlude_mel=True) else 'mel'

def is_valid_tab_name(name, exlude_mel=False):
    """
    Args:
        name (str)
        exlude_mel (bool, optional)

    Returns:
        (bool)

    Check if tab's title is MEL (unless exlude_mel == True), Python (also check
    if titles matches *.py or *.mel).
    """

    tabs_regex = kk.VALID_TABS_REGEX[2:] if exlude_mel else kk.VALID_TABS_REGEX
    return True if any(re.match(regex, name) for regex in tabs_regex) else False

def get_all_txt_edit_child_instances(cls, parent=None):
    """
    Args:
        cls (class)
        parent (QtWidgets.QWidget, optional)

    Find all <cls> instances in <parent> QTextEdits children (if no parent is set,
    Script Editor window will be taken as parent).
    """

    if not parent:
        parent = get_script_editor()

    if not parent:
        return None

    txt_edits = get_text_edits(parent)

    found = []

    for txt_edit in txt_edits or ():
        for child in txt_edit.children() or ():
            if isinstance(child, cls):
                found.append(child)

    return found

def get_child_instance(cls, parent=None, create_if_missing=True):
    """
    Args:
        cls (class)
        parent (QtWidgets.QWidget, optional)
        create_if_missing (bool, optional)

    Get <cls> instance from <parent> children. If no instance is found and
    <create_if_missing> is True, create a new <cls> instance and return it.
    """

    if parent is None:
        parent = get_maya_window()

    for child in parent.children() or ():
        if isinstance(child, cls):
            return child

    new_instance = None

    if create_if_missing:
        new_instance = cls(parent=parent)

    return new_instance

def get_manager(cls, parent=None):
    """
    Args:
        cls (managers.Manager derived class)
        parent (QtWidgets.QWidget, optional)

    Returns:
        (managers.Manager derived instance)

    Get existing <cls> manager from <parent> children. If no parent is set, Maya's
    main window will be used as parent. If no manager is found, a new one will
    be created and returned, with existing PreferencesManager (also created if
    not found).
    """

    if parent is None:
        parent = get_maya_window()

    manager = get_child_instance(cls, parent=parent, create_if_missing=False)

    if not manager:
        prefs = get_child_instance(PreferencesManager, parent=get_maya_window())
        manager = cls(parent=parent, prefs=prefs)

    return manager

def get_overlay(txt_edit, padding=True):
    """
    Args:
        txt_edit (QtWidgets.QTextEdit)

    Returns:
        (OverlayWidget)

    Create OverlayWidget on <txt_edit> if necessary and return it.
    """

    for child in txt_edit.children():
        if isinstance(child, OverlayWidget):
            return child

    overlay = OverlayWidget(txt_edit, padding)
    overlay.show()

    return overlay

def is_child_class_needed(widget, cls):
    """
    Args:
        widget (QWidget)
        cls (class or tuple(class))

    Returns:
        (bool)

    Check whether any of  <target_classes> is found in <widget>'s children or not.
    (used to detect if eventFilters, Highlighters, etc. are to be
    installed on widget)
    """

    for child in widget.children():
        if isinstance(child, cls):
            return False

    return True

def close_existing(parent, window_name):
    """
    Args:
        parent (QtWidgets.QWidget)

    Close existing window instance from <parent>'s children, looking for matching
	<window_name>.
    """

    for widget in parent.children() or ():
        if widget.objectName() == window_name:
            widget.setParent(None)
            widget.close()
            widget.deleteLater()
            del widget
            break

def get_indent_level(block, ignore_white_lines=False, keep_whitespaces=False):
    """
    Args:
		block (QtGui.QTextBlock)
		ignore_white_lines (bool=True)

	Returns:
		(int)

	Get <block>'s indentation-level.
	"""

    text = block.text()
    if not text:
        if ignore_white_lines:
            return None
        return 0

    non_whitespace_chars = re.search('\S', text)

    if keep_whitespaces and not non_whitespace_chars:
        return len(text)

    if ignore_white_lines and not non_whitespace_chars:
        return None
    if not non_whitespace_chars:
        return 0

    return non_whitespace_chars.start(0)

# def run_after_event(new_method, target_method):
#     """
#     Wrap both functions into a new one so <new_method> is called after
#     <target_method>.
#
#     Usage:
#         target_method = run_after_event(new_method, target_method)
#     """
#
#     def run_both(*args, **kwargs):
#         target_method(*args, **kwargs)
#         new_method(*args, **kwargs)
#
#     return run_both

# def run_before_event(new_method, target_method):
#     """
#     Wrap both functions into a new one so <new_method> is called before
#     <target_method>.
#
#     Usage:
#         target_method = run_before_event(new_method, target_method)
#     """
#     def run_both(*args, **kwargs):
#         new_method(*args, **kwargs)
#         target_method(*args, **kwargs)
#
#     return run_both
