"""
Python Snippets management file.
"""

import sys
import re

from maya import cmds

from PySide2 import QtCore

from custom_script_editor import utils
from custom_script_editor.snippets.common import Snippets
from custom_script_editor.objects_inspector import getdir


MAYA_CMDS = list(set(cmds.help('[a-z]*', list=True, lng='python')))
CMDS_REGEX = re.compile(r'(\bcmds|mc)\.\w+$')

PYTHON_ERRORS = [
    word
    for word in __builtins__
    if any(x in word.lower() for x in ['error', 'exception'])
]

CLASS_REGEX = QtCore.QRegExp('class\s+(\w+)')
DEF_REGEX = QtCore.QRegExp('def\s+(\w+)')


class PythonSnippets(Snippets):

    """
    Custom eventFilter installed on Maya's Script Editor to handle snippets.
    """

    def __init__(self, txt_edit, form_lay, default=False):
        super(PythonSnippets, self).__init__(txt_edit, form_lay, default=default)

        self._type = 'python'

    @utils.catch_error
    def get_custom_snippets(self, line_to_key):
        """
        Args:
            line_to_key (str)

        Returns:
            (list[str])

        Get exception and double underscore snippets.
        """

        word_to_key = line_to_key.split(' ')[-1]

        # get "python" custom snippets from ./custom_snippets.json file
        custom_snippets = super(PythonSnippets, self).get_custom_snippets()
        custom_snippets += self._get_exception_snippets(line_to_key)
        custom_snippets += self._get_double_underscore_snippets(word_to_key)

        return custom_snippets

    def _get_double_underscore_snippets(self, word_to_key):
        """
        Args:
            word_to_key (str)

        Returns:
            (list[str] or [])

        Get double underscore snippets if <word_to_key> starts with an "_" char .
        """

        if word_to_key.startswith('_'):
            return ['__main__', '__name__', '__class__', '__init__']

        return []

    @utils.catch_error
    def _get_exception_snippets(self, line_to_key):
        """
        Args:
            word_to_key (str)

        Returns:
            (list[str] or [])

        Get "Exception" snippets after "except".
        """

        words_before_key = re.findall('\w+\.*\w*', line_to_key)
        standard_except = 'except Exception as e:'

        if len(words_before_key) == 1:
            if standard_except.startswith(words_before_key[0]):
                return [standard_except]

        if len(words_before_key) != 2 or words_before_key[0] != 'except':
            return []

        to_lower = words_before_key[1].lower()
        errors = [
            x for x in PYTHON_ERRORS if x.lower().startswith(to_lower)
        ]

        return sorted([x +' as e:' for x in errors])

    @utils.catch_error
    def get_snippets(self, line, i, key):
        """
        Args:
            line (str)
            i (int)
            key (str)

        Returns:
            (list[str] or [])

        Get snippets list for current word.
        """

        word_before_key, word_to_key = self._get_tokens_before_key(line, i, key)

        if not word_to_key:         # for instance, on backspace erasing the first
            return None         # character of a word, we don't want all snippets to rise

        snippets = MAYA_CMDS if CMDS_REGEX.search(line[:i+1]) else []
        snippets += self._get_dir_snippets(word_before_key)

        if not snippets:
            snippets = self._get_modules_snippets()
            snippets.extend(self._get_text_snippets())

        if snippets:
            snippets = list(set(snippets))      # filter duplicate snippets
            snippets = sorted(snippets)         # sort snippets

        snippets += self.get_custom_snippets(line[:i] +key)
        snippets += self._get_super_snippets()

        # get snippet words that start with word_to_key (no case match)
        return self._get_matching_snippets(snippets, word_to_key)

    @utils.catch_error
    def _get_super_snippets(self):
        """
        Args:
            word (str)

        Get super(Class, self).func expression snippets.
        """

        cursor = self._txt_edit.textCursor()

        class_name = self._get_previous_declaration_name(cursor, 'class')
        def_name = self._get_previous_declaration_name(cursor, 'def')

        short_super = 'super({}, self)'.format(class_name)
        long_super = 'super({}, self).{}('.format(class_name, def_name)

        result = [short_super, long_super] if class_name and def_name else \
                 [short_super] if class_name and not def_name else \
                 []

        return result

    def _get_previous_declaration_name(self, cursor, decl_str):
        """
        Args:
            cursor (QTextCursor)
            decl_str (str) : "class" or "def"

        Retrieve current "class" or "def" name from previous lines.
        """

        doc = cursor.document()

        regex = CLASS_REGEX if decl_str == 'class' else DEF_REGEX
        search_cursor = doc.find(regex, cursor.position(), doc.FindBackward)

        line = search_cursor.block().text()
        if not line:
            return None

        return line.split(decl_str)[1].split('(')[0].split(':')[0].strip()

    @utils.catch_error
    def _get_modules_snippets(self):
        """
        Returns:
            (list[str])

        Get modules snippets from sys.
        """

        imported_modules = [x.split('.')[0] for x in sys.modules]
        return list(set(imported_modules))

    def _get_dir_snippets(self, word):
        """
        Returns:
            (list[str])

        Get dir(currentWord) as snippets ( removing the last '\.\w+' part )
        if error return empty list so the other snippets methods will be called.
        """

        if '.' in word:
            word = '.'.join(word.split('.')[:-1])

        result = getdir(word, silent=True)

        return result if result else []
