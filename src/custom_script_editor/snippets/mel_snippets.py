"""
Mel Snippets management file.
"""

from maya import cmds

from custom_script_editor import utils
from custom_script_editor.snippets.common import Snippets


MAYA_CMDS = list(set(cmds.help('[a-z]*', list=True, lng='mel')))


class MelSnippets(Snippets):

    """
    Custom eventFilter installed on Maya's Script Editor to handle snippets.
    """

    def __init__(self, txt_edit, form_lay, default=False):
        super(MelSnippets, self).__init__(txt_edit, form_lay, default=default)

        self._type = 'mel'

    @utils.catch_error
    def get_snippets(self, line, i, key):
        """
        Args:
            line (str)
            i (int)
            key (str)

        Returns:
            (list[str] or [])

        """

        _, word_to_key = self._get_tokens_before_key(line, i, key)

        if not word_to_key:
            return None

        snippets  = MAYA_CMDS
        snippets += self._get_text_snippets()
        snippets += self.get_custom_snippets()

        return self._get_matching_snippets(snippets, word_to_key)
