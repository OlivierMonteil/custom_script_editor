"""
Auto-completion Snippets management files for Mel and Python tabs.

The custom_script_editor.managers.SnippetsManager class is used to add this
system to the Script Editor tabs QTextEdit instances.

To do:
    User-Interface for custom snippets editing (custom_snippets.json file).
"""

# shorten absolute imports
from custom_script_editor.snippets.python_snippets import PythonSnippets
from custom_script_editor.snippets.mel_snippets import MelSnippets
