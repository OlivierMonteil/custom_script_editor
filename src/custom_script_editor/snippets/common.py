"""
Snippets base classes and methods.
"""

import json
import os
import re

from PySide2 import QtWidgets, QtCore, QtGui

from maya import cmds
import maya.OpenMayaUI as OMUI

import shiboken2 as shiboken

from custom_script_editor import constants as kk
from custom_script_editor import utils
from custom_script_editor.stylesheet import apply_style


CUSTOM_JSON = os.path.dirname(__file__).replace('\\', '/') +'/custom_snippets.json'
LAST_WORD_REGEX = re.compile(r'\b([\w\.]+)$')


class Snippets(QtCore.QObject):

    """
    Custom eventFilter installed on Maya's Script Editor to handle snippets.
    """

    def __init__(self, text_edit, form_lay, default=False):
        super(Snippets, self).__init__(parent=text_edit)

        self._txt_edit = text_edit
        self._form_lay = form_lay
        self._default = default
        self._snippets_box = None
        self._trigger_on_cursor_change = True
        self._box = None
        self._suspended = False
        self._type = None

        self._txt_edit.cursorPositionChanged.connect(self.on_cursor_change)

    def suspend(self, state):
        self._suspended = state

    def set_default(self, default):
        self._default = default

    def get_snippet_box(self):
        """
        Args:
            form_lay (str)

        Returns:
            (str)

        Get 'Snippets' menu's chechbox if not already found (taking in account
        a potentiel delay between Snippets instance creation and popup-menu's).
        """

        # snippet box already found
        if not self._snippets_box is None:
            return self._snippets_box

        se_popup_menus = utils.get_script_editor_popup_menus()
        popup_menu = [menu for menu in se_popup_menus if self._form_lay in menu]

        if not popup_menu:
            return None

        # set self._snippets_box value
        self._snippets_box = '{}|{}|{}'.format(
            popup_menu[0], kk.CUSTOM_MENU_NAME, kk.SNIPPETS_BOX_NAME
        )

        return self._snippets_box

    def set_enabled(self, state):
        self.set_default(state)

        snippets_box = self.get_snippet_box()

        if snippets_box is None:
            return

        if not cmds.checkBox(snippets_box, q=True, ex=True):
            return

        cmds.checkBox(snippets_box, e=True, value=state)

    def is_suspended(self):
        return self._suspended

    def install(self):
        self._txt_edit.installEventFilter(self)

    def is_enabled(self):
        """
        Returns:
            (bool)

        Get Custom Menu > Snippets box checked state.

        (cmds.menuItem(self._snippets_box, q=True, checkBox=True) seems to fail,
        so we have to get menuItem as QAction to get the right result)
        """

        if not cmds.formLayout(self._form_lay, q=True, ex=True):
            return False

        if self._suspended:
            return False

        snippets_box = self.get_snippet_box()
        ptr = OMUI.MQtUtil.findMenuItem(snippets_box)
        if not ptr:
            return self._default

        try:
            qt_box = shiboken.wrapInstance(long(ptr), QtWidgets.QAction)
        except:
            return False

        return qt_box.isChecked()

    def eventFilter(self, obj, event):
        # close snippet box on RMB and MMB
        # (LMB seems to be handled directly into QTextCursor class)

        event_type = event.type()

        if not cmds.formLayout(self._form_lay, q=True, ex=True):
            self.deleteLater()
            return True

        # close snippet box on mouse click
        if event_type == QtCore.QEvent.MouseButtonPress:
            self._kill_box()
            return False

        # handle snippet box on key press events
        if event_type == QtCore.QEvent.KeyPress:
            key = event.key()
            text = event.text()
            # skip if key is not a char key
            if not text or not text in kk.CHARACTERS +kk.SPECIAL_CHARS:
                if not key in [
                    QtCore.Qt.Key_Return,
                    QtCore.Qt.Key_Down,
                    QtCore.Qt.Key_Up,
                    QtCore.Qt.Key_Backspace
                ]:
                    return False

            if not self.is_enabled():
                self._kill_box()
                return False

            self._trigger_on_cursor_change = False

            cursor = self._txt_edit.textCursor()

            if self._box:
                if key == QtCore.Qt.Key_Return:
                    self.validate_snippet(self._box.current_item().text())
                    return True

                if key == QtCore.Qt.Key_Down:
                    self._box.go_down()
                    return True
                if key == QtCore.Qt.Key_Up:
                    self._box.go_up()
                    return True

                # close snippet box (will be reconstructed later if necessary)
                self._kill_box()

            to_lower = True
            if event.modifiers():
                if event.modifiers() == QtCore.Qt.ShiftModifier:
                    to_lower = False
                else:
                    return False

            key_as_str = QtGui.QKeySequence(key).toString()
            key_as_str = key_as_str.lower() if to_lower else key_as_str

            # skip if key as string is more than one character (ex: space)
            # except for Backspace, so the snippets would still be triggered
            if key != QtCore.Qt.Key_Backspace and len(key_as_str) > 1:
                return False

            cursor = self._txt_edit.textCursor()
            line = cursor.block().text()

            try:
                 # on alphabetical characters
                if re.match('[a-zA-Z_]', key_as_str) or key_as_str.lower() in ['.', 'backspace']:
                    pos = cursor.positionInBlock()
                    if key_as_str.lower() == 'backspace':
                        pos = max(0, pos -1)
                        key_as_str = ''

                    found_snippets = self.get_snippets(line, pos, key_as_str)

                    # if snippets have been found, show SnippetBox under current word
                    if found_snippets:
                        rect = self._txt_edit.cursorRect()
                        pos = QtCore.QPoint(rect.x() +35, rect.y() +15)

                        self._box = SnippetBox(self._txt_edit, found_snippets, pos)
                        self._box.itemPressed.connect(lambda x: self.validate_snippet(x.text()))
                        self._box.show()

            except:
                pass

        return False

    def validate_snippet(self, text):
        """
        Args:
            text (str)

        Insert selected completion into QTextEdit and close the snippets box.
        """

        cursor = self._txt_edit.textCursor()
        pos = cursor.position()
        cursor.movePosition(cursor.StartOfWord, cursor.MoveAnchor)

        if cursor.position() == pos:
            cursor.movePosition(cursor.PreviousWord, cursor.MoveAnchor)

        cursor.movePosition(cursor.EndOfWord, cursor.KeepAnchor)
        cursor.insertText(text)

        cmds.evalDeferred(self._kill_box, lowestPriority=True)

    def _kill_box(self):
        """
        Close the snippets box.
        """

        if not self._box is None:
            self._box.close()

        self._box = None

    @utils.catch_error
    def get_custom_snippets(self):
        """
        Returns:
            (list[str] or [])

        Get user snippets from custom_snippets.json.
        """

        if self._type is None:
            return []

        try:
            with open(CUSTOM_JSON, 'r') as opened_file:
                content = json.load(opened_file)
                return content[self._type]
        except:
            return []

    def _get_tokens_before_key(self, line, i, key):
        match = LAST_WORD_REGEX.search(line[:i])
        if not match:
            return None, None

        word_before_key = match.group(1)
        word_to_key = word_before_key.split('.')[-1] +key

        return word_before_key, word_to_key

    def _get_matching_snippets(self, snippets, word_to_key):
        snippets = list(set(snippets))

        return [
            s for s in snippets
            if s.lower().startswith(word_to_key.lower())
            and word_to_key.lower() != s.lower()
        ]

    @utils.catch_error
    def get_snippets(self, line, i, key):
        """
        Args:
            line (str)
            i (int)
            key (str)

        Returns:
            (list[str] or [])

        Needs to be re-implemented.
        """

        return []

    def _get_text_snippets(self):
        """
        Returns:
            (list[str])

        Return words from current tab with length > 3.

        TO DO: buffer system rather than parsing the whole text at each key press.
        """

        text = self._txt_edit.toPlainText()
        return [x for x in re.findall('[\w]+', text) if len(x) >3]

    def on_cursor_change(self):
        """ Close the snippets box if users clicks into the QTextEdit. """
        if self._trigger_on_cursor_change:
            if self._box:
                self._kill_box()

        else:
            self._trigger_on_cursor_change = True


class SnippetBox(QtWidgets.QWidget):

    """
    Custom QWidget with QListWidget that will hold completion words, and arrow keys loop
    (up at row 0 will set selection at the end and reverse)
    """

    itemPressed = QtCore.Signal(QtWidgets.QListWidgetItem)

    def __init__(self, parent, list, pos):
        super(SnippetBox, self).__init__(parent)

        self.setStyleSheet('border : transparent;')
        apply_style(self)

        lay = QtWidgets.QVBoxLayout(self)
        self.view = QtWidgets.QListWidget()
        lay.addWidget(self.view)

        for word in list:
            self.add_item(word)

        self.move(pos)
        self.set_current_item(self.item(0))

        for w in [self, self.layout()]:
            w.setContentsMargins(0, 0, 0, 0)

        view_width = self.size_hint_for_column(0) + 2 * self.frame_width()
        view_height = self.size_hint_for_row(0) * self.count() + 2 * self.frame_width()
        self.setMaximumSize(view_width +16, view_height +2)

        self.view.itemPressed.connect(self.itemPressed.emit)

    def add_item(self, name):
        return self.view.addItem(name)

    def current_item(self):
        return self.view.currentItem()

    def set_current_item(self, item):
        return self.view.setCurrentItem(item)

    def item(self, index):
        return self.view.item(index)

    def frame_width(self):
        return self.view.frameWidth()

    def size_hint_for_column(self, col):
        return self.view.sizeHintForColumn(col)

    def size_hint_for_row(self, row):
        return self.view.sizeHintForRow(row)

    def set_current_row(self, row):
        return self.view.setCurrentRow(row)

    def current_row(self):
        return self.view.currentRow()

    def count(self):
        return self.view.count()

    def go_down(self):
        """
        Go an item down. If current item is already the last one, got to the top.
        """

        currRow = self.current_row()
        if currRow +1 < self.count():
            self.set_current_row(currRow +1)
        else:
            self.set_current_row(0)

    def go_up(self):
        """
        Go an item up. If current item is already the first one, got to the bottom.
        """

        currRow = self.current_row()
        if currRow -1 > 0:
            self.set_current_row(currRow -1)
        else:
            self.set_current_row(self.count() -1)
