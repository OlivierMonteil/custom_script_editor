"""
`Script Editor > HotBox > Custom Script Editor > Custom Tools` menu files.

All *.py script files found (except for the `__init__.py` one) will automatically
be added to this menu if they include a `run()` function at their top level.
Also, if a CUSTOM_MENU_LABEL constant is set into a script file,
its value will be set as the associated menu action's title.
"""
