import sys

from PySide2 import QtWidgets, QtCore, QtGui

from core import get_code_blocks


WINDOW_TITLE = 'Script organizer'
WINDOW_OBJECT_NAME = 'CSEScriptOrganizer'

BG_COLOR        = QtGui.QColor(29,  34,  46)
CLASS_COLOR     = QtGui.QColor(228, 187, 106)
FUNC_COLOR      = QtGui.QColor(92,  166, 237)
COMMENTED_COLOR = QtGui.QColor(90,  99,  111)
SELECT_COLOR    = QtGui.QColor(67,  252, 162)
INDENT = 25

WIDTH = 500
HEIGHT = 30
VERTICAL_SPACE = 6


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.setWindowTitle('Script Organizer')
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        mainWidget = QtWidgets.QWidget(self)
        mainLay = QtWidgets.QVBoxLayout(mainWidget)
        self.setCentralWidget(mainWidget)

        self.view = View(self)
        self.scene = Scene(self.view, self)
        self.view.setScene(self.scene)

        mainLay.addWidget(self.view)

        for w in (self, mainWidget):
            w.setContentsMargins(0, 0, 0, 0)
        mainLay.setContentsMargins(4, 4, 4, 4)

        # self.scene.clicked.connect(self.node_clicked.emit)

    def parse_text(self, txt):
        items_by_name = {}

        for block in get_code_blocks(txt):
            parent = block.parent
            if parent:
                parent = items_by_name[parent.name]

            item = self.add_item(block, parent)
            items_by_name[block.name] = item

    def add_item(self, block, parent):
        if block.type == 'def':
            new_item = Function(block, parent)
        elif block.type == 'class':
            new_item = Class(block, parent)
        else:
            raise ValueError('Unknown node type : "{}"'.format(block.type))

        self.scene.addItem(new_item)

        y_value = len(self.scene.items())*(VERTICAL_SPACE +HEIGHT)
        if parent:
            y_value -= parent.absolute_y()

        new_item.setY(y_value)

        return new_item


class View(QtWidgets.QGraphicsView):
    def __init__(self, parent=None):
        super(View, self).__init__(parent)

        self.setViewportUpdateMode(self.FullViewportUpdate)
        self.setRenderHints(QtGui.QPainter.Antialiasing | QtGui.QPainter.SmoothPixmapTransform)
        self.setTransformationAnchor(self.AnchorUnderMouse)
        self.setDragMode(self.RubberBandDrag)
        self.scale(0.8, 0.8)

        self.setBackgroundBrush(QtGui.QBrush(BG_COLOR, QtCore.Qt.SolidPattern))

class Scene(QtWidgets.QGraphicsScene):

    clicked = QtCore.Signal()

    def __init__(self, view, parent=None):
        QtWidgets.QGraphicsScene.__init__(self, parent)

        self._view = view
        self.setItemIndexMethod(self.NoIndex)

        self.selectionChanged.connect(self._on_selection_changed)

    def get_underimouse_item(self, scenePos):
        return self.itemAt(scenePos, self._view.transform())

    def update_all(self):
        for item in self.items() or ():
            item.update()

    def _on_selection_changed(self):
        for item in self.items() or ():
            if item.isSelected():
                item.setZValue(10)
                item.setFlag(item.ItemStacksBehindParent, False)
            else:
                item.setZValue(item.block.indent +1)
                item.setFlag(item.ItemStacksBehindParent, True)

        self.update_all()




class Code(QtWidgets.QGraphicsItem, QtCore.QObject):

    color = QtGui.QColor(85, 85, 85)

    def __init__(self, block, parent=None, indented=True):
        QtWidgets.QGraphicsItem.__init__(self, parent)
        QtCore.QObject.__init__(self)

        self.setFlag(self.ItemIsMovable)
        self.setFlag(self.ItemIsSelectable)
        self.setFlag(self.ItemSendsGeometryChanges)
        self.setZValue(-block.indent-1)

        self.block = block
        self.parent_node = parent
        self._children = []
        self._width = WIDTH
        self._height = HEIGHT

        if parent:
            self.setX(INDENT)       # relative to parent
            self.setFlag(self.ItemStacksBehindParent)


    def width(self):
        return self._width

    def height(self):
        return self._height

    def absolute_y(self):
        return self.y() +self.parent_node.absolute_y() if self.parent_node else self.y()

    def combined_height(self):
        return self._height +sum([x.combined_height() for x in self._children])

    def itemChange(self, change, value):
        if change == self.GraphicsItemChange.ItemPositionHasChanged:
            pos = QtCore.QPointF(INDENT, value.y()) if self.parent_node else \
                  QtCore.QPointF(0, value.y())

            self.setX(pos.x())
            return pos

        return super(Code, self).itemChange(change, value)

    def boundingRect(self):
        """ Qt re-implementation, determines the node's bouding box as QRect. """

        return QtCore.QRect(
            -self.width()/2.0,
            -self._height/2.0 -50,
            self.width(),
            self._height +50
        )

    def shape(self):
        """ Qt re-implementation, determines the node's shape as QPainterPath. """

        path = QtGui.QPainterPath()
        rect = self.boundingRect()
        path.addRoundedRect(
            rect.x() +1,
            rect.y() +51,
            rect.width() -2 -self.block.indent*INDENT,
            rect.height() -48,
            10,
            10
        )
        return path

    def paint(self, painter, option, widget):
        """
        Args:
            painter (QPainter)
            option (QStyleOptionGraphicsItem)
            widget (QWidget)

        Qt re-implementation, paints the node itself.
        """

        shape = self.shape()
        rect = self.boundingRect()

        # add selection border
        if self.isSelected():
            # broaden the QPainter's clipping rectangle
            rect = option.rect
            option.rect = QtCore.QRect(
                rect.x() -10,
                rect.y() -10,
                rect.width() +20,
                rect.height() +20
            )
            painter.setClipRect(option.rect)
            # paint the border
            painter.setPen(QtGui.QPen(SELECT_COLOR, 8))
            painter.drawPath(shape)

        # paint the node itself
        color = COMMENTED_COLOR if self.block.commented else self.color
        painter.setBrush(color)
        painter.setPen(QtGui.QPen(QtCore.Qt.black, 2))
        painter.drawPath(shape)

        # set node's name font
        font = painter.font()
        font.setBold(True)
        font.setPointSize(11)
        font.setLetterSpacing(font.PercentageSpacing, 110)
        painter.setFont(font)

        painter.setClipRect(self.boundingRect())
        painter.drawText(
            -self.width()/2.0 +30,
            self._height/2.0 -6,
            str(self.block).split(':')[-2]
        )

        if self.block.decorated:
            painter.drawText(
                -self.width()/2.0 +10,
                self._height/2.0 -6,
                '@'
            )

class Class(Code):
    color = CLASS_COLOR

class Function(Code):
    color = FUNC_COLOR


def run():
    app = QtWidgets.QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec_())

def test():
    test_file = __file__
    with open(test_file, 'r') as opened_file:
        test_text = opened_file.read()

    app = QtWidgets.QApplication(sys.argv)

    window = MainWindow()
    window.parse_text(test_text)
    window.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    test()
