import re


REGEX = re.compile(r'[^\S\r\n]*\#*[^\S\r\n]*(\@|class|def)\s+(\w+)\s*\(([\w,\.\s]*)\):')
DECORATORS_REGEX = re.compile(r'^\s*\#*\s*\@\w+')
INDENT_REGEX = re.compile('^\s*')
COMMENT_REGEX = re.compile('^\s*#')
NON_WHITESPACE_REGEX = re.compile('\S')

BLOCK_TYPES = ['class', 'def']


def get_code_blocks(txt):
    """
    Args:
        txt (int)

    Yields:
        (BlockData)
    """

    last_block = None

    for match in REGEX.finditer(txt):
        match_txt = match.group(0)
        indent = INDENT_REGEX.search(match_txt)
        indent = len(indent.group(0))//4 if indent else 0
        start, length, commented = get_block_length(txt, match.start(0), indent)

        data = BlockData(
            match.group(1),
            match.group(2),
            match.group(3),
            start,
            length,
            indent,
            commented,
            bool(COMMENT_REGEX.search(match_txt))
        )

        while last_block:
            if indent > last_block.indent:
                data.setParent(last_block)
                break

            last_block = last_block.parent

        last_block = data

        yield data

def get_block_length(txt, start, indent):
    """
    Args:
		txt (str)
		start (int)
        indent (int)

	Returns:
		(int, int)

    Get code block's real start (in case of decorators above) and length.
	"""

    # look next indents to get block lenth
    all_lines = txt[start:].split('\n')

    while not NON_WHITESPACE_REGEX.search(all_lines[0]):
        start += len(all_lines[0] +'\n')
        all_lines = all_lines[1:]

    length = len(all_lines[0] +'\n')

    got_end = False
    commented = False

    for i, line in enumerate(all_lines[1:-1]):
        if DECORATORS_REGEX.search(line) or REGEX.search(line):
            got_end = True
            break

        line_indent = get_indent_level(line)

        if line_indent is None:
            length += len(line +'\n')
            continue

        if line_indent > indent:
            length += len(line +'\n')

        else:
            got_end = True
            break

    if not got_end:
        length += len(all_lines[-1])

    # look back for decorators
    past_lines = txt[:start].split('\n')[:-1]

    while past_lines:
        previous_line = past_lines[-1]
        if DECORATORS_REGEX.search(previous_line):
            line_length = len(previous_line +'\n')
            start -= line_length
            length += line_length
            past_lines = past_lines[:-1]
            commented = True
        else:
            break

    return start, length, commented

def get_indent_level(line):
    """
    Args:
		line (str)

	Returns:
		(int)

	Get <line>'s indentation-level.
	"""

    if not line:
        return None

    non_whitespace = NON_WHITESPACE_REGEX.search(line)
    return non_whitespace.start(0) if non_whitespace else None


class BlockData(object):
    def __init__(self, block_type, name, args, start, length, indent, decorated, commented):

        self.type = block_type
        self.name = name
        self.args = [x.strip() for x in args.split(',')]
        self.start = start
        self.length = length
        self.indent = indent
        self.commented = commented
        self.parent = None
        self.children = []

        self.decorated = decorated

    def __str__(self):
        res = str(self.parent) if self.parent else ''
        return  res + '{} {}({}):'.format(
            self.type,
            self.name,
            ', '.join(self.args)
        )

    def cmp(self):
        return str(self.parent) +str(self) if self.parent else str(self)

    def __lt__(self, other):
        return self.cmp() < other.cmp()

    def __le__(self, other):
        return self.cmp() <= other.cmp()

    def __gt__(self, other):
        return self.cmp() > other.cmp()

    def __ge__(self, other):
        return self.cmp() >= other.cmp()

    def setParent(self, parent):
        self.parent = parent
        parent.children.append(self)




def test():
    test_file = r'C:\Users\Olivier\Documents\maya\scripts\custom_script_editor\src\custom_script_editor\highlight\python_highlight.py'
    with open(test_file, 'r') as opened_file:
        test_text = opened_file.read()

    blocks = []

    for block in get_code_blocks(test_text):
        # print ('"' +test_text[block.start:block.start +block.length] +'"')
        pass

if __name__ == '__main__':
    test()
