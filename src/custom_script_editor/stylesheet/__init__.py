"""
GUIs style management files. All the ./.css/*.css files found will be concatenated
and % strings replacements will be done from colors.json dict to get the complete
stylesheet content.
"""

# shorten absolute imports
from custom_script_editor.stylesheet.handler import apply as apply_style
