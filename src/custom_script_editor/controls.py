"""
Controls widgets for palettes/prefs editing.
"""

import os

from PySide2 import QtWidgets, QtCore, QtGui

from custom_script_editor import constants as kk
from custom_script_editor.prefs import data


DIRTY_COLOR = (255, 150, 50)


                ###########################################
                #         Palettes Editor controls        #
                ###########################################


class AttributeWidget(QtWidgets.QWidget):
    """
    Widget for attributes editing.
    """

    color_changed = QtCore.Signal()
    enabled_changed = QtCore.Signal()
    link_changed = QtCore.Signal(object, object)
    create_link_asked = QtCore.Signal()

    def __init__(self, attr, label, value, enabled, tooltip, *args, **kwargs):
        """
        Args:
            attr (str)
            label (str)
            value (tuple(float)) : attribute's color
            enabled (bool)
            tooltip (str)

        Initiates self.
        """

        super(AttributeWidget, self).__init__(*args, **kwargs)

        self.attr = attr
        self.label = label
        self._link = None
        self._alternate_color = False
        self._hovered = False

        self._startup_link = None
        self._startup_value = value
        self._startup_enabled = True if enabled is None else enabled

        layout = QtWidgets.QHBoxLayout(self)

        # attribute cannot be disabled, like "background"
        if enabled is None:
            self._label_widget = QtWidgets.QLabel(label, self, toolTip=tooltip)

        # attribute may be enabled/disabled
        else:
            self._label_widget = CheckBox(
                label, self, toolTip=tooltip, checked=enabled
            )

        # add link icon as custom QLabel
        self._link_icon = LinkIcon(self.has_link(), self)
        # add color button
        self._color_button = ColorButton(
            tuple(value),
            self
        )

        # populate layout
        layout.addWidget(self._label_widget)
        layout.addWidget(self._link_icon)
        layout.addWidget(self._color_button)

        # set contents margins
        self.setContentsMargins(0, 0, 0, 0)
        layout.setContentsMargins(2, 2, 2, 2)

        if enabled is False:
            self._color_button.setEnabled(False)
            self._link_icon.setEnabled(False)

        # update initial state and connect signals
        if isinstance(self._label_widget, CheckBox):
            self._label_widget.stateChanged.connect(self._on_checkbox_changed)
            # update visually enabled states
            self._label_widget.update()
            self._link_icon.update()

        # connect signals
        self._color_button.color_changed.connect(self._on_color_changed)

        self._color_button.installEventFilter(self)

        self._dirty_handler = DirtyHandler(self._label_widget)

        self.map_methods()

    def on_linked_color_changed(self):
        """ Update button's color on link source color changed. """
        self._color_button.override_rgb(self.sender().value())

        self.color_changed.emit()

    def _on_color_changed(self):
        self.color_changed.emit()
        self._update_dirty_state()

    def _on_checkbox_changed(self, state):
        """
        Args:
            state (bool)

        Update link icon and color button's enabled state and emit "color_changed"
        signal so the text sample highlight will be updated.
        """

        self._color_button.setEnabled(state)
        self._link_icon.setEnabled(state)

        self.enabled_changed.emit()
        self._update_dirty_state()

    def set_enabled(self, state):
        """
        Args:
            state (bool)

        Set attribute's enabled <state> if has QCheckBox (attributes like
        "background" cannot be disabled).
        """

        # widget cannot be disabled
        if isinstance(self._label_widget, QtWidgets.QLabel):
            return

        return self._label_widget.setChecked(state)

    def is_enabled(self):
        """
        Returns:
            (bool)

        Get the enabled state of the attribute.
        """

        # widget cannot be disabled
        if isinstance(self._label_widget, QtWidgets.QLabel):
            return True

        return self._label_widget.isChecked()

    def _update_dirty_state(self, *args):
        """
        Returns:
            (bool)

        Check if any of attribute's color or link has changed since startup.
        """

        self._dirty_handler.set(self.is_dirty())

    def _reset_button(self):
        self._color_button.set_rgb(self._startup_value)
        self.set_link(self._startup_link)
        self.set_enabled(self._startup_enabled)

        self._update_dirty_state()

    def is_dirty(self):
        """
        Returns:
            (bool)

        Check if any of attribute's color or link has changed since startup.
        """

        value_dirty = self.value(ignore_override=True) != tuple(self._startup_value)
        enabled_dirty = self._startup_enabled != self.is_enabled()
        link_dirty = self._link != self._startup_link

        return value_dirty or enabled_dirty or link_dirty

    def has_checkbox(self):
        """
        Returns:
            (bool)

        Check whether attribute may be enabled/disabled by a checkbox or not.
        """

        return True if isinstance(self._label_widget, CheckBox) else False

    def value(self, ignore_override=False):
        """
        Returns:
            (tuple(float))

        Get color button's value.
        """

        if ignore_override:
            return self._color_button.non_overriden_rgb

        return self._color_button.rgb

    def set_link(self, link, startup=False):
        """
        Args:
            link (AttributeWidget)

        Set attribute's source <link> and emit link_changed(old, new) signal.
        """

        old_link = self._link
        self._link = link

        # update link icon
        self._link_icon.set_linked(self.has_link())
        # update link tooltip
        if link is not None:
            self._link_icon.set_tooltip(link.label)

        if startup:
            self._startup_link = link

        # emit signal to update links signal connections and event filters
        self.link_changed.emit(old_link, link)

        self._update_dirty_state()

    def has_link(self):
        """
        Returns:
            (bool)

        Return True if attribute has an input link.
        """

        return True if self._link else False

    def get_link_as_str(self):
        """
        Returns:
            (str)

        Return input link's attribute name.
        """

        return self._link.attr if not self._link is None else None

    def under_mouse(self):
        """
        Returns:
            (bool)

        Check if self is under mouse cursor.
        """

        region = self.visibleRegion()
        cursor_pos = self.mapFromGlobal(QtGui.QCursor().pos())

        return region.contains(cursor_pos)

    def eventFilter(self, obj, event):
        """
        Qt re-implementation, display warning and prevent mouse clicks on color
        button if widget has is linked to another.
        """

        if not hasattr(event, 'type') or not hasattr(event, 'button'):
            return False

        # filter double-click events on linked color buttons
        if event.type() == QtCore.QEvent.MouseButtonDblClick:
            self._link_icon.set_warning_state(True)
            return True

        # filter LMB press events on linked color buttons
        if event.type() == QtCore.QEvent.MouseButtonPress:
            if event.button() == QtCore.Qt.LeftButton:
                if self.has_link():
                    self._link_icon.set_warning_state(True)
                    return True

        # filter LMB release events on linked color buttons
        elif event.type() == QtCore.QEvent.MouseButtonRelease:
            if event.button() == QtCore.Qt.LeftButton:
                if self.has_link():
                    self._link_icon.set_warning_state(False)
                    return True

        return False

    def contextMenuEvent(self, event):
        """
        Qt re-implementation, handle context menu on RMB click.
        """

        # skip if widget is disabled or not disablable ("background" and "normal"
        # should not be linked attributes)
        if not self.is_enabled() or not self.has_checkbox():
            return

        menu = QtWidgets.QMenu(self)

        reset_action = menu.addAction('Reset')


        # linked attributes menu
        if self.has_link():
            break_link_action = menu.addAction(
                'Break link from "{}"'.format(self._link.label)
            )
            create_link_action = None

        else:
            create_link_action = menu.addAction('Set input link...')
            break_link_action = None

        action = menu.exec_(self.mapToGlobal(event.pos()))
        if not action:
            return

        if action == reset_action:
            self._reset_button()

        elif action == break_link_action:
            self.set_link(None)
            self._link_icon.set_linked(False)

        elif action == create_link_action:
            self.create_link_asked.emit()



    def set_hovered(self, state):
        """
        Args:
            (bool)

        Set self._hovered to <state> and repaint self.
        """

        self._hovered = state
        self.update()

    def add_left_padding(self, level):
        """
        Args:
            level (int)

        Add left padding on label to align label to page indentation level.
        """

        left_padding = 10*(level -1)

        if isinstance(self._label_widget, QtWidgets.QLabel):
            self._label_widget.setStyleSheet(
                'padding-left: {};'.format(left_padding)
            )

        else:
            self._label_widget.left_padding = left_padding
            self._label_widget.update()

    def set_alternate_color(self):
        """ Set attribute's background to be painted with the alternate color. """
        self._alternate_color = True

    def paintEvent(self, event):
        """
        Qt re-implementation, paints self (used to paint alternate color, else
        this function will call the super() paintEvent).
        """

        super(AttributeWidget, self).paintEvent(event)

        if not self._hovered and not self._alternate_color:
            return

        # set QPainter
        painter = QtGui.QPainter(self)

        if self._alternate_color:
            if self._hovered:
                painter.setBrush(QtGui.QColor(255, 255, 255, 40))
            else:
                painter.setBrush(QtGui.QColor(255, 255, 255, 10))
        else:
            if self._hovered:
                painter.setBrush(QtGui.QColor(255, 255, 255, 40))
            else:
                painter.setBrush(QtCore.Qt.transparent)

        if self._hovered:
            painter.setPen(
                QtGui.QPen(
                    QtGui.QColor(190, 190, 200, 80),
                    22,
                    QtCore.Qt.SolidLine,
                    QtCore.Qt.RoundCap,
                    QtCore.Qt.RoundJoin
                )
            )

        else:
            painter.setPen(QtCore.Qt.NoPen)

        # set paint rectangle (a bit wider than event.rect())
        draw_offset = QtCore.QPoint(-10, -10)
        draw_rect = event.rect()
        draw_rect.setTopLeft(draw_offset)
        draw_rect.setBottomRight(draw_rect.bottomRight() -draw_offset)

        # paint rectangle
        painter.drawRect(draw_rect)

    def map_methods(self):
        """ Simplify self._color_button methods access. """

        self.override_rgb = self._color_button.override_rgb
        self.remove_override = self._color_button.remove_override


class ColorButton(QtWidgets.QPushButton):
    """
    Custom QPushButton for color picking.
    """

    counter = 0
    color_changed = QtCore.Signal()
    warning_asked = QtCore.Signal(bool)

    def __init__(self, rgb, *args, **kwargs):
        """
        Args:
            rgb (tuple(float))

        Initiates self.
        """

        super(ColorButton, self).__init__(*args, **kwargs)

        self._rgb = None
        self._overriden_rgb = None

        self.setMaximumSize(16, 16)
        self.setObjectName('ColorButton' +str(self.counter))

        ColorButton.counter += 1
        if rgb:
            self.set_rgb(rgb)

        self.clicked.connect(self.open_color_picker)


    @property
    def rgb(self):
        """
        Get button's color, or override one's if set.
        """

        rgb = self._overriden_rgb if not self._overriden_rgb is None else self._rgb
        return tuple(rgb)

    @property
    def non_overriden_rgb(self):
        """
        Get non-overriden button's color.
        """

        return tuple(self._rgb)

    def set_rgb(self, rgb):
        """
        Args:
            rgb (tuple(float))

        If <rgb> is different from the current one, set button's color and emit
        "color_changed" QtCore.Signal().
        """

        if rgb != self._rgb:
            self._rgb = tuple(rgb)
            self.color_changed.emit()

            self.update_color()

    def override_rgb(self, rgb):
        """
        Args:
            rgb (tuple(float))

        Set override color for button (its origin color is kept in memory, but
        the override one is display instead). This method is used in case of
        color links. When the link is broken, the origin color is set back.

        ("color_changed" QtCore.Signal() is emitted so the text highlight and
        attributes linked to this one will also be updated)
        """

        self._overriden_rgb = tuple(rgb)
        self.update_color()

        self.color_changed.emit()

    def remove_override(self):
        """
        Remove override color for this button and set the origin color back.

        ("color_changed" QtCore.Signal() is emitted so the text highlight and
        attributes linked to this one will also be updated)
        """

        self._overriden_rgb = None
        self.update_color()

        self.color_changed.emit()

    def update_color(self):
        """
        Update button's color using stylesheet, displaying override color if set.
        """

        rgb = self.rgb      # self._rgb or self.override_rgb if not None

        self.setStyleSheet(
            'QPushButton#%s {background-color: rgb(%s, %s, %s)}' % (
                self.objectName(), rgb[0], rgb[1], rgb[2]
            )
        )

    def paintEvent(self, event):
        """ Qt re-implementation, paint the button itself. """

        # display a red cross instead if button is disabled
        if not self.isEnabled():
            # scale down paint rect
            offset = QtCore.QPoint(3, 3)
            rect = event.rect()
            rect.setTopLeft(offset)
            rect.setBottomRight(rect.bottomRight() -offset)

            painter = QtGui.QPainter(self)
            painter.setPen(QtGui.QPen(QtGui.QColor(217, 82, 82), 2))
            # craw the cross
            painter.drawLine(rect.topLeft(), rect.bottomRight())
            painter.drawLine(rect.topRight(), rect.bottomLeft())

        # paint noramally if enabled
        else:
            super(ColorButton, self).paintEvent(event)

    def open_color_picker(self):
        """
        Open a QColorDialog and edit ColorButton's color if user picjed a new color.
        (emits color_changed signal in set_rgb() so the QTextEdit palette will be
        updated)
        """

        dialog = QtWidgets.QColorDialog(self)
        dialog.setOption(dialog.DontUseNativeDialog)
        dialog.setWindowTitle('Select {} color'.format(self.parent().label))

        # set ColorButton's color as dialog's current one
        qcolor = QtGui.QColor(*self._rgb)
        dialog.setCurrentColor(qcolor)
        # add ColorButton's color to dialog's custom colors if not already in
        if not any(dialog.customColor(i) == qcolor for i in range(dialog.customCount())):
            for i in reversed(range(dialog.customCount())):
                dialog.setCustomColor(i+1, dialog.customColor(i))
            dialog.setCustomColor(0, qcolor)

        # run dialog
        if dialog.exec_():
            new_rgb = dialog.currentColor()
            new_rgb = [new_rgb.red(), new_rgb.green(), new_rgb.blue()]
            self.set_rgb(new_rgb)


class CheckBox(QtWidgets.QCheckBox):
    """
    Custom QCheckBox with checked state display on text (unchecked box's text
    will be displayed as disabled).
    """

    def __init__(self, *args, **kwargs):
        super(CheckBox, self).__init__(*args, **kwargs)

        self.left_padding = None

        p = self.palette()
        self._disabled_color = p.color(p.Disabled, p.Text)
        self._enabled_color = p.color(p.Normal, p.Text)

        self.stateChanged.connect(lambda x: self.update())

    def update(self, *args, **kwargs):
        """
        Qt re-implementation, edit text color depending on the checked state
        using stylesheet.
        """

        qcolor = self._disabled_color if not self.isChecked() else self._enabled_color

        # style = 'QCheckBox {color: rgb(%s, %s, %s);' \
        #         % (qcolor.red(), qcolor.green(), qcolor.blue())
        #
        # if self.left_padding:
        #     style += 'padding-left: {};'.format(self.left_padding)
        #
        # style += '}'
        #
        # self.setStyleSheet(style)

        p = self.palette()
        p.setColor(p.Text, qcolor)
        self.setPalette(p)

        super(CheckBox, self).update(*args, **kwargs)


class LinkIcon(QtWidgets.QLabel):
    """
    Attributes link icon.
    """

    def __init__(self, linked=False, parent=None):
        super(LinkIcon, self).__init__('', parent)

        self.setFixedSize(16, 16)

        self._linked = linked
        self._warning = False

    def set_linked(self, linked):
        """
        Args:
            linked (bool)

        Set QLabel's linked state and force update().
        """

        self._linked = linked
        self.update()

    def set_warning_state(self, state):
        """
        Args:
            state (bool)

        Display link as red icon if <state> is True (called on LMB events on
        linked color buttons, has no effect if the QLabel is disabled).
        """

        self._warning = state
        self.update()

    def set_tooltip(self, input_attr):
        """
        Args:
            input_attr (str)

        Display <input_attr> in tooltip.
        """

        if input_attr is None:
            self.setToolTip(None)

        else:
            self.setToolTip('input: {}'.format(input_attr))

    def paintEvent(self, event):
        """
        Qt re-implementation, paint the QLabel's icon, depending on linked and
        warning states.
        """

        if not self._linked:
            return

        # set current map to be painted
        link_map = 'link' if not self._warning else 'link_warn'
        if not self.isEnabled():
            link_map = 'link_disabled'

        # draw the image
        painter = QtGui.QPainter(self)
        painter.drawImage(
            event.rect(),
            QtGui.QImage(os.path.join(kk.ICONS_ROOT, '{}.png'.format(link_map)))
        )


class Frame(QtWidgets.QWidget):
    """
    Collapsible frame with title for attribute editor sections.
    """

    def __init__(self, title, parent):
        super(Frame, self).__init__(parent)

        lay = QtWidgets.QVBoxLayout(self)
        self._main_widget = QtWidgets.QWidget(self)
        self.lay = QtWidgets.QVBoxLayout(self._main_widget)

        tokens = title.split('|')
        self._button = FrameButton(tokens[-1], self, level=len(tokens))

        lay.addWidget(self._button)
        lay.addWidget(self._main_widget)

        # few size rules
        for w in (self, lay, self._main_widget):
            w.setContentsMargins(0, 0, 0, 0)
        self.lay.setMargin(0)
        self.lay.setSpacing(0)
        self._button.setFixedHeight(20)

        # connect signal
        self._button.clicked.connect(self.toggle_frame)

    def toggle_frame(self):
        """ "Toggle" frame by showing/hiding its main widget. """

        if self._main_widget.isVisible():
            self.collapse()
        else:
            self.expand()

    def collapse(self):
        """ "Collapse" frame by hiding its main widget. """

        self._main_widget.setVisible(False)
        self._button.set_expanded(False)

    def expand(self):
        """ "Expand" frame by showing its main widget. """

        self._main_widget.setVisible(True)
        self._button.set_expanded(True)

    def button(self):
        """
        Returns:
            (FrameButton)

        Get frame's button instance.
        """

        return self._button


class FrameButton(QtWidgets.QPushButton):
    """
    Expand/collapse flat button with title and expand/collapse arrow.
    """

    def __init__(self, title, parent=None, level=0):
        super(FrameButton, self).__init__(parent)

        self._expanded = True

        self._title = title
        self._level = level

        self._expandedIcon = QtGui.QPixmap(os.path.join(kk.ICONS_ROOT, 'arrow_down.png'))
        self._collapsedIcon = QtGui.QPixmap(os.path.join(kk.ICONS_ROOT, 'arrow_right.png'))

        self._background = self._get_background_color()

    def level(self):
        """
        Returns:
            (int)

        Get the indentation level for self (in case of nested frames).
        """

        return self._level

    def set_expanded(self, expanded):
        """
        Args:
            expanded (bool)
        """

        self._expanded = expanded

    def is_expanded(self):
        """
        Returns:
            (bool)
        """

        return self._expanded

    def _get_background_color(self):
        """
        Returns:
            (QtGui.QColor)

        Get background's color depending on the level of the frame.
        """

        color = max(95 -15*self._level, 50)
        return QtGui.QColor(color, color, color)

    def paintEvent(self, event):
        """ Qt re-implementation, paints the widget itself. """

        rect = event.rect()
        painter = QtGui.QPainter(self)

        # paint flat background
        painter.fillRect(rect, self._background)

        pixmap = self._expandedIcon if self._expanded else self._collapsedIcon
        rect.setLeft(rect.left() +4 +10*(self._level -1))

        # paint arrow
        painter.drawPixmap(rect.left(), rect.height()/2-6, 12, 12, pixmap)
        # paint text
        painter.drawText(rect.left() +16, rect.height()/2 +4, self._title)


                ###########################################
                #       Preferences Editor controls       #
                ###########################################


class AttrCheckBox(QtWidgets.QCheckBox):
    """
    Custom QCheckBox. Input data dict is modified on checked state changes.
    """

    def __init__(self, rule, attr, _data, parent=None):
        """
        Args:
            rule (str)
            attr (str)
            _data (dict)
            parent (QtWidgets.QWidget)

        Initiates self.
        """

        super(AttrCheckBox, self).__init__(attr, parent)

        self._attr = attr
        self._rule = rule
        self._data = _data

        # set sartup checked state from config data
        self.setChecked(self._data.get_by_names(rule, attr))
        # connect signal to dict's update
        self.stateChanged.connect(self.update_data)

        self.dirty_state = DirtyHandler(self)

    def update_data(self, state):
        """
        Args:
            state (QtCore.Qt.CheckState)

        Update data and dirty state on checkbox's state change.
        """

        # get checked state as bool as the input QtCore.Qt.CheckState might be
        # 0 (Unchecked), 1 (PartiallyChecked), or 2 (Checked)
        state = False if not state else True

        self._data.set_by_names(self._rule, self._attr, state)
        self.dirty_state.set(self._data.is_attr_dirty(self._rule, self._attr))


class ComboBox(QtWidgets.QWidget):
    """
    Custom QWidget with QLabel and QComboBox. Input data dict is modified on
    combobox changes.
    """

    def __init__(self, rule, attr, _data, parent=None):
        """
        Args:
            rule (str)
            attr (str)
            _data (dict)
            parent (QtWidgets.QWidget)

        Initiates self.
        """

        super(ComboBox, self).__init__(parent)

        self._data = _data
        self._rule = rule
        self._attr = attr

        self.lay = QtWidgets.QHBoxLayout(self)

        label = QtWidgets.QLabel(attr, self)
        self.box = QtWidgets.QComboBox(self)

        self.lay.addWidget(label)
        stretched_row = 1

        # add color-wheel palette's button if necessary (signal must be connected
        # outside this file so we won't get circular import with palette_editor.py)
        if attr == 'palette' and rule != 'expressions':
            self.button = IconButton('palette', label)
            self.lay.addWidget(self.button)

            stretched_row = 2

        self.lay.addWidget(self.box)
        self.lay.setStretch(stretched_row, 1)

        self.setContentsMargins(4, 4, 4, 4)
        self.lay.setContentsMargins(0, 0, 0, 0)
        label.setMinimumWidth(60)
        self.setFixedHeight(28)

        # set items list at startup
        if attr == 'palette':
            rule_palettes = data.get_themes_list(rule)
            for pal in rule_palettes:
                self.box.addItem(pal)
        elif attr == 'profile':
            rule_profiles = data.get_profiles_list(rule)
            for prof in rule_profiles:
                self.box.addItem(prof)

        # set sartup item from config data
        self.box.setCurrentText(self._data.get_by_names(rule, attr))
        # connect signal to dict's update
        self.box.currentTextChanged.connect(self.update_data)

        self.dirty_state = DirtyHandler(label)

    def update_data(self, txt):
        """
        Args:
            txt (str)

        Update data and dirty state on combobox's currentText change.
        """

        self._data.set_by_names(self._rule, self._attr, txt)
        self.dirty_state.set(self._data.is_attr_dirty(self._rule, self._attr))


class IconButton(QtWidgets.QPushButton):
    """
    Simple icon button with normal/hover/pressed/disabled icons set in paintEvent.
    """

    def __init__(self, icon_name, parent=None, size=14, **kwargs):
        super(IconButton, self).__init__(parent, **kwargs)

        self._icon_name = icon_name
        self._size = size
        self.setFixedSize(size, size)

    def paintEvent(self, event):
        """ Qt re-implementation, paints the button itself. """

        # set map
        map_name = '{}.png'.format(self._icon_name)

        if not self.isEnabled():
            map_name = '{}_disabled.png'.format(self._icon_name)
        elif self.isDown():
            map_name = '{}_pressed.png'.format(self._icon_name)
        elif self.underMouse():
            map_name = '{}_hover.png'.format(self._icon_name)

        map_path = os.path.join(kk.ICONS_ROOT, map_name)

        # set anti-aliased painter for self
        painter = QtGui.QPainter(self)
        painter.setRenderHint(painter.SmoothPixmapTransform)
        # paint map
        painter.drawImage(
            QtCore.QRect(0, 0, self._size, self._size),
            QtGui.QImage(map_path)
        )

        painter.end()


class DirtyHandler(object):
    """ Dirty state display management class. """

    custom_names_count = 0

    def __init__(self, widget):


        self._widget = widget
        self._widget.setProperty("dirtyDisplay", False)
        # self._widget_id = self._get_widget_id()

    def set(self, dirty):
        """
        Args:
            dirty (bool)

        Edit widget's text color depening on <dirty> state, using stylsheet.
        """

        self._widget.setProperty("dirtyDisplay", dirty)
        style = self._widget.style()
        style.unpolish(self._widget)
        style.polish(self._widget)
        self._widget.update()

        # # apply dirty color
        # if dirty:
        #     self._widget.setStyleSheet('%(id)s {color: rgb%(color)s;}' % {
        #             'color': DIRTY_COLOR,
        #             'id': self._widget_id
        #         }
        #     )
        #
        # # reset stylesheet
        # else:
        #     self._widget.setStyleSheet('')

    # def _get_widget_id(self):
    #     """
    #     Returns:
    #         (str)
    #
    #     Get stylsheet's ID selector for self._widget (from type "WidgetClass#objectName").
    #     """
    #
    #     # make sure that the widget has an objectName set and get it
    #     object_name = self._get_object_name()
    #     inherited_classes = self._widget.__class__.__mro__
    #
    #     for cls in inherited_classes:
    #         # the first PySide2.QtWidgets... class should be the "closest"
    #         # inherited one
    #         if 'PySide' in str(cls):
    #             return '{}#{}'.format(cls.__name__, object_name)
    #
    # def _get_object_name(self):
    #     """
    #     Returns:
    #         (str)
    #
    #     Get self._widget's object name. If not set, set a custom one and return
    #     it, so we can set its stylesheet using ID selector.
    #     """
    #
    #     object_name = self._widget.objectName()
    #
    #     # object name is not set
    #     if not object_name:
    #         # create unique custom name for the widget
    #         object_name = 'DirtyHandlerCustomName{}'.format(
    #             DirtyHandler.custom_names_count
    #         )
    #         # set object's name
    #         self._widget.setObjectName(object_name)
    #
    #         DirtyHandler.custom_names_count += 1
    #
    #     return object_name
