"""
Maya's Script Editor's tabs QtCore.Qt.QKey inputs management :

- Ctrl +Shift +D : lines duplication
- Ctrl +UP/DOWN : move lines
- Ctrl +Alt +UP/DOWN : add new multi-cursor up/down
- Ctrl +/ : toggle blocks comment
- Ctrl +V : (multi-paste enabled)
- embracing characters : (), {}, [], ``, "", ''

The custom_script_editor.managers.KeysManager class is used to add this
system to the Script Editor tabs QTextEdit instances.
"""

# shorten absolute imports
from custom_script_editor.keys.handler import KeysHandler
