"""
Custom QMenu that will add QActions for each script from ./src/tools folder that has
a `run()` function in it (see `ToolsMenu.add_scripts`).
"""

import os
import re
import sys

from PySide2 import QtWidgets, QtGui

from custom_script_editor import constants as kk


TOOLS_ROOT = os.path.join(os.path.dirname(__file__), 'tools')
TOOLS_ICONS_ROOT = os.path.join(TOOLS_ROOT, 'icons')
TOOLS_IMPORT_PATH = 'custom_script_editor.tools'
SKIPPED_FILES = ['__init__.py']


class ToolsMenu(QtWidgets.QMenu):

    def __init__(self, parent=None):
        super(ToolsMenu, self).__init__(parent, toolTipsVisible=True)

        self.actions = []

        # add all *.py scripts with run() function in it
        self.add_scripts()

    def has_actions(self):
        """ Returns: (bool) """
        return True if self.actions else False

    def add_scripts(self):
        """
        Add all *.py scripts with a `run` function in it to the menu.
        If constant `CUSTOM_MENU_LABEL` is set into the file, its value will be set
        as the QAction's text.
        """

        scripts_list = self.get_scripts_list()

        for script_name in scripts_list:
            module_path = '{}.{}'.format(TOOLS_IMPORT_PATH, script_name[:-3])
            # we will keep track of whether module has been imported here so we
            # can remove it if not added to the menu
            imported_there = False

            # import module
            if module_path not in sys.modules:
                new_module = __import__(
                    module_path,
                    locals(),
                    globals(),
                    [TOOLS_IMPORT_PATH]
                )

                imported_there = True

            else:
                new_module = sys.modules[module_path]

            if not hasattr(new_module, 'run'):
                # remove module from sys.module if has been imported there and
                # is not going to be added to the menu
                if imported_there:
                    del new_module
                    del sys.modules[module_path]
                continue

                    ###############################################
                    #           add new QAction to menu           #
                    ###############################################

            label = getattr(
                new_module,
                'CUSTOM_MENU_LABEL',
                '{}.run()'.format(script_name[:-3])     # default value if not found
            )

            script_action = self.addAction(label, new_module.run)
            script_doc = new_module.__doc__

            # remove trailing line breaks
            if script_doc:
                while script_doc.startswith('\n'):
                    script_doc = script_doc[1:]
                while script_doc.endswith('\n'):
                    script_doc = script_doc[:-1]

                # set script's docstring as action's tooltip
                script_action.setToolTip(script_doc)

            # set icon if found
            icon_path = get_script_icon(script_name)
            if icon_path:
                script_action.setIcon(QtGui.QIcon(icon_path))

            # add QAction to menu
            self.actions.append(script_action)

    def get_scripts_list(self):
        """
        Returns:
            (list[str]) : *.py files shortname

        Get *.py files into tools directory (or main.py files from found packages).
        """

        scripts = []

        for item in os.listdir(TOOLS_ROOT) or ():
            if item.endswith('.py') and item not in SKIPPED_FILES:
                scripts.append(item)
                continue

            path = os.path.join(TOOLS_ROOT, item)

            # get packages main.py files
            if os.path.isdir(path):
                if 'main.py' in os.listdir(path):
                    scripts.append('{}.{}'.format(item, 'main.py'))

        return scripts


def get_script_icon(script_name):
    """
    Args:
        (str)

    Returns:
        (str or None) : absolute file's path

    Get the corresponding <script_name>.ext file path if found into icons folder.
    """

    script_name = script_name.split('.')[0]

    for item in os.listdir(TOOLS_ICONS_ROOT):
        if re.match('^%s\.\w+$' % script_name, item):
            return os.path.join(TOOLS_ICONS_ROOT, item)



def run(pos, parent=None):
    """
    Args:
        pos (QtCore.QPoint)
        parent (some widget)

    Run ToolsMenu at <pos> (only if some valid scripts are found, see
    `ToolsMenu.add_scripts`).
    """

    menu = ToolsMenu(parent)

    # run ToolsMenu only if some valid scripts are found
    if menu.has_actions():
        menu.exec_(pos)

    else:
        menu.setParent(None)
        menu.deleteLater()

        print (kk.INFO_MESSAGE.format(
                'no valid script found into {}.'.format(TOOLS_ROOT)
            )
        )
