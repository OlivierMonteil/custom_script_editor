"""
Customization managers classes (mainly used to detect if some feature is needed
on a widget and to apply it if so).

All managers are refering to the same Preferences Manager class, which allows
them to get synchronized on preferences changes by receiving its "changed"
QtCore.Signal, emited on preferences save.
Also, they are all parented to Maya's main window, and should be called only
once by session.
"""

from PySide2 import QtCore, QtGui, QtWidgets

from custom_script_editor import utils
from custom_script_editor import constants as kk
from custom_script_editor import dialogs
# Preferences imports
from custom_script_editor.prefs import data
# Multi-cursors imports
from custom_script_editor.multi_cursor import MultiCursorHandler
# snippets imports
try:
    from custom_script_editor.snippets import PythonSnippets
    from custom_script_editor.snippets import MelSnippets
except:
    pass
# block-collapse imports
from custom_script_editor.blocks_collapse import CollapseHandler
from custom_script_editor.console_filter import FilterHandler, FilterData
# keys imports
from custom_script_editor.keys import KeysHandler
# highlight imports
from custom_script_editor.highlight import ConsoleHighlighter, ConsolePalette
from custom_script_editor.highlight import MelHighlighter, MelPalette
from custom_script_editor.highlight import PythonHighlighter, PythonPalette
from custom_script_editor.highlight import QuickHelpDelegate, QuickHelpPalette, QuickHelpHiglighter


try:
    SNIPPETS_CLASSES_BY_NAME = {
        'mel': MelSnippets,
        'python' : PythonSnippets
    }

    SNIPPETS_CLASSES = SNIPPETS_CLASSES_BY_NAME.values()
except:
    pass

HIGHLIGHT_CLASSES_BY_NAME = {
    'console': ConsoleHighlighter,
    'mel': MelHighlighter,
    'python' : PythonHighlighter
}


class Manager(QtCore.QObject):
    """
    Managers base class.
    """

    def __init__(self, prefs=None, enabled=True, parent=None):
        super(Manager, self).__init__(parent=parent)

        self.setParent(parent)

        self._handler_class = None
        self._enabled = enabled

        self._prefs = data.PreferencesManager(parent=parent) if prefs is None else prefs
        self._prefs.changed.connect(self.on_prefs_changed)

        print (
            kk.INFO_MESSAGE.format(
                '{} instance created.'.format(self.__class__.__name__)
            )
        )

    def on_prefs_changed(self):
        print (
            '# [{}] info : preferences changed (not implemented yet).'.format(
                self.__class__.__name__
            )
        )

    def apply(self, txt_edit, *args):
        msg  = '# [{}] error : either handler_class variable or apply() method '
        msg += 'must be re-implemented.'

        if self._handler_class is None:
            print (msg.format(self.__class__.__name__))
            return

        handler = self._handler_class(*args, parent=txt_edit)
        handler.install()

    def is_needed(self, txt_edit):
        return utils.is_child_class_needed(txt_edit, self._handler_class)


class FilterManager(Manager):
    def __init__(self, prefs=None, enabled=True, parent=None):
        super(FilterManager, self).__init__(
            prefs=prefs,
            enabled=enabled,
            parent=parent
        )

        self._handler_class = FilterHandler
        self._filter_data = FilterData()

    def apply(self, txt_edit, *args):
        handler = self._handler_class(self._filter_data, parent=txt_edit)
        handler.install()

    def is_enabled(self):
        return self._prefs.get_by_names("console-filter", "enabled")

    def is_needed(self, txt_edit):
        return super(FilterManager, self).is_needed(txt_edit) and self.is_enabled()


class MultiCursorManager(Manager):

    _prefs_key = 'multi-cursors'

    def __init__(self, prefs=None, enabled=True, parent=None):
        super(MultiCursorManager, self).__init__(
            prefs=prefs,
            enabled=enabled,
            parent=parent
        )

        self._handler_class = MultiCursorHandler

    def is_enabled(self, tab_type):
        return self._prefs.get_by_names(tab_type, "enabled") and \
               self._prefs.get_by_names(tab_type, self._prefs_key)

    def is_needed(self, txt_edit, tab_type):
        return super(MultiCursorManager, self).is_needed(txt_edit) and self.is_enabled(tab_type)


class KeysManager(Manager):

    _prefs_key = 'hotkeys'

    def __init__(self, prefs=None, enabled=True, parent=None):
        super(KeysManager, self).__init__(
            prefs=prefs,
            enabled=enabled,
            parent=parent
        )

        self._handler_class = KeysHandler

    def is_enabled(self, tab_type):
        return self._prefs.get_by_names(tab_type, "enabled") and \
               self._prefs.get_by_names(tab_type, self._prefs_key)

    def is_needed(self, txt_edit, tab_type):
        return super(KeysManager, self).is_needed(txt_edit) and self.is_enabled(tab_type)


class BlocksCollapseManager(Manager):

    _prefs_key = "blocks-collapse"

    def __init__(self, prefs=None, enabled=True, parent=None):
        super(BlocksCollapseManager, self).__init__(
            prefs=prefs,
            enabled=enabled,
            parent=parent
        )

        self._handler_class = CollapseHandler

    def is_enabled(self, tab_type):
        return self._prefs.get_by_names(tab_type, "enabled") and \
               self._prefs.get_by_names(tab_type, self._prefs_key)

    def is_needed(self, txt_edit, tab_type):
        return super(BlocksCollapseManager, self).is_needed(txt_edit) and self.is_enabled(tab_type)

    # def apply(self, txt_edit):
    #     widget = CollapseWidget(txt_edit)
    #     widget.show()
    #
    # def is_needed(self, txt_edit):
    #     return utils.is_child_class_needed(txt_edit, CollapseWidget)


class SnippetsManager(Manager):
    """
    Mel/Python Snippets manager.
    """

    _prefs_key = "snippets"

    def apply(self, txt_edit, form_lay, from_type):
        new_snippets = SNIPPETS_CLASSES_BY_NAME[from_type](
            txt_edit,
            form_lay,
            default=self._prefs.get_by_names(from_type, 'snippets')
        )

        QtCore.QTimer.singleShot(500, new_snippets.install)

    def on_prefs_changed(self):

        msg = '# [SnippetsManager] info : {} default "Snippets" enabled state set to {}".'

        for rule in ('mel', 'python'):
            if self._prefs.is_attr_dirty(rule, 'snippets'):
                default_state = self._prefs.get_by_names(rule, 'snippets')
                self.set_default_by_type(rule, default_state)

                print (msg.format(rule, default_state))

    def set_default_by_type(self, by_type, value):
        for obj in utils.get_all_txt_edit_child_instances(SNIPPETS_CLASSES_BY_NAME[by_type]) or ():
            obj.set_default(value)

    def is_enabled(self, tab_type):
        return self._prefs.get_by_names(tab_type, "enabled") and \
               self._prefs.get_by_names(tab_type, self._prefs_key)

    def is_needed(self, txt_edit, tab_type):
        if not self.is_enabled(tab_type):
            return False
        return utils.is_child_class_needed(txt_edit, tuple(SNIPPETS_CLASSES))


class HighlightManager(Manager):
    """
    Highlighters, palettes and profiles manager (parented to Maya's main window).
    """

    data_changed = QtCore.Signal(dict)

    def __init__(self, prefs=None, enabled=True, parent=None):
        super(HighlightManager, self).__init__(prefs=prefs, enabled=enabled, parent=parent)

        # create palettes instances
        self._console_palettes = {
            'console': ConsolePalette(
                self,
                self._prefs.get_palette_data('console'),
                self._prefs.get_profile_data('console')
            ),
            'python': PythonPalette(
                self,
                self._prefs.get_palette_data('console|python-highlight'),
                self._prefs.get_profile_data('console|python-highlight')
            ),
            'mel': MelPalette(
                self,
                self._prefs.get_palette_data('console|mel-highlight'),
                self._prefs.get_profile_data('console|mel-highlight')
            )
        }

        self._python_palette = PythonPalette(
            self,
            self._prefs.get_palette_data('python'),
            self._prefs.get_profile_data('python')
        )

        self._mel_palette = MelPalette(
            self,
            self._prefs.get_palette_data('mel'),
            self._prefs.get_profile_data('mel')
        )

        self._help_palette = QuickHelpPalette(
            self,
            self._prefs.get_palette_data('quick-help'),
            self._prefs.get_profile_data('quick-help')
        )

        self._expr_palette = MelPalette(
            self,
            self._prefs.get_palette_data('expressions|mel-highlight'),
            self._prefs.get_profile_data('expressions|mel-highlight')
        )

        self._all_palettes = [
            self._console_palettes['console'],
            self._console_palettes['python'],
            self._console_palettes['mel'],
            self._python_palette,
            self._mel_palette,
            self._help_palette,
            self._expr_palette
        ]

    def apply(self, txt_edit, from_type, padding=False):
        """
        Args:
            txt_edit (QtWidgets.QTextEdit)
            from_type (str)

        Apply new <from_type> highlighter on <txt_edit> and connect the
        corresponding profile data changes to highlighter's update.
        """

        from_type = from_type.split('|')[0]

        self.remove_current_highlight(txt_edit)

        # ConsoleHighlighter needs console palette AND both Mel and Python's ones.
        if from_type == 'console':
            palette = self.get_palette_by_name('console')
            mel_palette = self.get_palette_by_name('console-mel')
            python_palette = self.get_palette_by_name('console-python')

            highlighter = HIGHLIGHT_CLASSES_BY_NAME[from_type](
                txt_edit,
                palette,
                mel_palette,
                python_palette,
                padding=padding
            )

            # for some reason, ConsoleHighlighter may lose all its attributes,
            # in this case, it will emit "corrupted" QtCore.Signal(), and we need
            # to reset its attributes.
            # Obviously this is not a very elegant way of fixing this issue, so
            # fixing the source of this problem should be investigated...
            highlighter.corrupted.connect(
                lambda : self.reset_console_highlighter(txt_edit)
            )
            palette = (palette, mel_palette, python_palette)

        elif from_type == 'quick-help':
            palette = self.get_palette_by_name(from_type)

            if isinstance(txt_edit, QtWidgets.QTextEdit):
                highlighter = QuickHelpHiglighter(txt_edit, palette)
            else:

                txt_edit.setItemDelegate(None)
                highlighter = QuickHelpDelegate(palette, txt_edit)
                txt_edit.setItemDelegate(highlighter)
                # txt_edit.repaint()

        # Mel and Python higlighters
        else:
            palette = self.get_palette_by_name(from_type)

            if from_type == 'expressions':
                palette = self.get_palette_by_name('expressions-mel')
                from_type = 'mel'

            highlighter = HIGHLIGHT_CLASSES_BY_NAME[from_type](
                txt_edit,
                palette,
                padding=padding
            )

        if isinstance(palette, (list, tuple)):
            for p in palette:
                p.data_changed.connect(highlighter.on_palette_change)
        else:
            palette.data_changed.connect(highlighter.on_palette_change)

        # connect common signal for re-highlight on palette/profile edits
        highlighter.needs_rehighlight.connect(self.on_re_higlight_asked)

        return highlighter

    def remove_current_highlight(self, widget):
        """
        Args:
            widget (QWidget)

        Remove QtGui.QSyntaxHighlighter from <widget>.
        """

        for syntax_hl in widget.findChildren(QtGui.QSyntaxHighlighter):
            syntax_hl.setDocument(None)

    def reset_console_highlighter(self, txt_edit):
        """
        Args:
            txt_edit (QtWidgets.QTextEdit)

        Reset ConsoleHighlighter's Painter if lost (see self.apply_highlighter()
        comments).
        """

        highlighter = self.sender()
        highlighter.reset(
            txt_edit,
            self.get_palette_by_name('console'),
            self.get_palette_by_name('console-mel'),
            self.get_palette_by_name('console-python'),
            padding=False
        )

        print (kk.INFO_MESSAGE.format('Console-higlight had to be reset.'))

    def on_re_higlight_asked(self):
        """
        Trigger Highlighter that emitted "on_re_higlight_asked" QtCore.Signal if
        self.is enabled only.
        """

        if self._enabled:
            highlighter = self.sender()
            highlighter.rehighlight()

    def on_prefs_changed(self):
        """
        Manager re-implementation, handle highlight palettes updates on preferences
        changed.
        """

        rules = [p.get_type() for p in self._all_palettes]

        to_be_updated = []

        # get dirty rules from preferences "profile" and "palette" names
        for rule in rules:
            for attr in ('profile', 'palette'):
                if self._prefs.is_attr_dirty(rule, attr):
                    to_be_updated.append(rule)
                    break

        if to_be_updated:
            # prompt user for re-highlight
            refresh_asked = dialogs.highlights_update_prompt(to_be_updated)

            # get current enabled state for further restoration (see below)
            state = self._enabled
            # enable/disable self (and so, re-highlights) depending on user's answer
            self.set_enabled(refresh_asked)

            # update dirty palettes
            for rule in to_be_updated:
                self.update_palette(rule)

            # set enabled state back as previously set
            self.set_enabled(state)

    def update_palette(self, rule, silent=False):
        """
        Args:
            rule (str)

        Update <rule>'s palette (getting both profile and palette's theme data
        from .json files). Associated Highlighters will be refreshed on user's
        demand (see self.on_prefs_changed).
        """

        theme = self._prefs.get_by_names(rule, 'palette')
        profile = self._prefs.get_by_names(rule, 'profile')

        self.set_rule_theme(rule, theme)
        self.set_rule_profile(rule, profile)

        if not silent:
            msg = '{} highlight updated.'.format(rule) if not 'higlight' in rule \
                  else '{} updated.'.format(rule)

            print (kk.INFO_MESSAGE.format(msg))

    def is_enabled(self, tab_type):
        return self._prefs.get_by_names(tab_type, "enabled")

    def is_needed(self, widget, tab_type):
        """
        Args:
            widget (QtWidgets.QWidget) : most likely a QTextEdit
            tab_type (str)

        Returns:
            (bool)

        Returns whether widget already has a custom Highlighter from this
        custom_script_editor's package or not.
        """

        if not self.is_enabled(tab_type):
            return False

        for x in widget.children():
            if isinstance(x, (ConsoleHighlighter, MelHighlighter, PythonHighlighter, QuickHelpDelegate)):
                return False

        return True

    def set_enabled(self, enabled):
        """ Args: enabled (bool) """
        self._enabled = enabled

    def set_rule_theme(self, rule, theme):
        """
        Args:
            rule (str)
            theme (str)

        Update <rule>'s palette data (will automatically trigger re-highlight if
        HighlightManager is enabled) from <theme>.json file content.
        """

        palette = self.get_palette_by_name(rule)
        palette_data = data.get_palette_data(rule, theme)

        palette.set(palette_data)

    def set_rule_profile(self, rule, name):
        """
        Args:
            rule (str)
            name (str)

        Update <rule>'s profile data (will automatically trigger re-highlight if
        HighlightManager is enabled) from <name>.json profile content.
        """

        profile = self.get_profile_by_name(rule)
        profile_data = data.get_profile_data(rule, name)

        profile.set(profile_data)

    def get_palette_by_name(self, from_type):
        """
        Args:
            from_type (str)

        Returns:
            (profiles.Palette derived class)

        Get palette instance by <from_type> rule name.
        """

        if 'console' in from_type:
            if 'python' in from_type:
                return self._console_palettes['python']
            elif 'mel' in from_type:
                return self._console_palettes['mel']
            else:
                return self._console_palettes['console']

        if 'expressions' in from_type:
            return self._expr_palette
        if from_type == 'quick-help':
            return self._help_palette
        if 'python' in from_type:
            return self._python_palette
        if 'mel' in from_type:
            return self._mel_palette

    def get_profile_by_name(self, from_type):
        """
        Args:
            from_type (str)

        Returns:
            (profiles.Profile)

        Get profile instance by <from_type> rule name.
        """

        palette = self.get_palette_by_name(from_type)
        return palette.profile()

    def set_palette_by_name(self, data, from_type):
        """
        Args:
            data (dict)
            from_type (str)

        Set palette's <data> by <from_type> rule name.
        """

        self.get_palette_by_name(from_type).set(data)

    def set_profile_by_name(self, data, from_type):
        """
        Args:
            data (dict)
            from_type (str)

        Set profile's <data> by <from_type> rule name.
        """

        self.get_profile_by_name(from_type).set(data)
