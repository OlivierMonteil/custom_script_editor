"""
Script Editor's customization package, allows :

    - syntax highlight
    - auto-completion "Snippets"
    - multi-cursor editing
    - few hotkeys
    - class/def blocks collapse/expand
"""

__author__ = 'Olivier Monteil'
__version__ = '2.2.0'
