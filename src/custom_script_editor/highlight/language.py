"""
Console language detection methods. Used to determine the appropriate highlight
for console lines.
"""

import re

MEL_LOOPS_REGEX = r'%s\s*\(.+\)\s*\{'
MEL_FUNCTIONS = r'%s\s*\(.+\)'
PYTHON_LOOPS_REGEX = r'%s\s*.*\s*:'


class Detector(object):
    """
    Language detection class for console content.
    """

    def __init__(self):

        self._mel_rules = [
            MatchRule('.+;$'),                                  # ; at the end of the line
            MatchRule(r'^\s*/\*'),                              # /* docstrings (start)
            MatchRule(r'^\s*(global\s+)*proc\s+'),              # proc declarations
            MatchRule(r'\$.+\s*\='),                            # $variable declaration
            MatchRule("^\s*//"),                                # comment lines
            SearchRule(r'\btrue\b'),                            # true
            SearchRule(r'\bfalse\b'),                           # false
            SearchRule(r'\bnone\b'),                            # none
            SearchRule(MEL_LOOPS_REGEX % 'for'),                # for (...) { declaration
            SearchRule(MEL_LOOPS_REGEX % 'if'),                 # if (...) { declaration
            SearchRule(MEL_LOOPS_REGEX % 'else'),               # else (...) { declaration
            SearchRule(MEL_LOOPS_REGEX % 'else if'),            # else if (...) { declaration
            SearchRule(MEL_LOOPS_REGEX % 'while'),              # while (...) { declaration
            SearchRule(MEL_FUNCTIONS % 'catch'),                # catch (...) declaration
            SearchRule(MEL_FUNCTIONS % 'catchQuiet')            # catchQuiet (...) declaration
        ]

        self._python_rules = [
            MatchRule(r'^\s*"""'),                              # """ docstrings
            MatchRule(r"^\s*'''"),                              # ''' docstrings
            MatchRule('from(.)+import(.)+'),                    # "from" imports
            MatchRule('import(.)+'),                            # imports
            SearchRule(r'(\bdef\b\s*)(_*\w+_*)'),               # function declarations
            SearchRule(r'(\bclass\b\s*)(_*\w+_*)'),             # class declarations
            MatchRule(r'\s*\@\w+\s*'),                          # decorators
            MatchRule(r'^\s*#'),                                # comment lines
            SearchRule(r'\bTrue\b'),                            # True
            SearchRule(r'\bFalse\b'),                           # False
            SearchRule(r'\bNone\b'),                            # None
            SearchRule(PYTHON_LOOPS_REGEX % 'for'),             # for (...) : declaration
            SearchRule(PYTHON_LOOPS_REGEX % 'if'),              # if (...) : declaration
            SearchRule(PYTHON_LOOPS_REGEX % 'else'),            # else (...) : declaration
            SearchRule(PYTHON_LOOPS_REGEX % 'elif'),            # else if (...) : declaration
            SearchRule(PYTHON_LOOPS_REGEX % 'while'),           # while (...) : declaration
            SearchRule(PYTHON_LOOPS_REGEX % 'try'),             # try (...) : declaration
            SearchRule(PYTHON_LOOPS_REGEX % 'except'),          # except (...) : declaration
            SearchRule(PYTHON_LOOPS_REGEX % 'finally'),         # finally (...) : declaration
            SearchRule(r'\s*print\s*(?!\()')                    # print call without ()
        ]

    def get_language(self, line):
        """
        Alternate Python and MEL rule-checks, so we won't perform all MEL checks
        on a Python line that would have been detected early.
        """

        mel_length = len(self._mel_rules)
        python_length = len(self._python_rules)

        for i in range(max(mel_length, python_length)):
            if i < mel_length and self._mel_rules[i].is_verified(line):
                return 'mel'

            if i < python_length and self._python_rules[i].is_verified(line):
                return 'python'

        return None


class MatchRule(object):
    """ Compiled regex re.match-based rule. """

    def __init__(self, regex):

        self._regex = re.compile(regex)

    def is_verified(self, line):
        """ Args: line (str)  Returns: (bool) """
        return False if self._regex.match(line) is None else True


class SearchRule(object):
    """ Compiled regex re.search-based rule. """

    def __init__(self, regex):

        self._regex = re.compile(regex)

    def is_verified(self, line):
        """ Args: line (str)  Returns: (bool) """
        return False if self._regex.search(line) is None else True
