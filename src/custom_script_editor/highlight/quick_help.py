"""
Custom QItemDelegate and QSyntaxHighlighter for Quick Help list syntax highlight.
"""


from PySide2 import QtWidgets, QtGui, QtCore

from custom_script_editor.highlight.common import Regex


class QuickHelpDelegate(QtWidgets.QItemDelegate):
    """
    Custom QItemDelegate for Quick Help's QListView. Uses a synthax highlighter
    to paint each item.
    """

    needs_rehighlight = QtCore.Signal()

    def __init__(self, palette, parent=None):
        super(QuickHelpDelegate, self).__init__(parent)

        self._txt_document = QtGui.QTextDocument(self)
        self._highlight = QuickHelpHiglighter(self._txt_document, palette)
        self._palette = palette

        self.set_enabled = self._highlight.set_enabled

        qpalette = parent.palette()
        # set no brush on highlighted text (will keep sinthax highlight)
        qpalette.setBrush(
            QtGui.QPalette.HighlightedText,
            QtGui.QBrush(QtCore.Qt.NoBrush)
        )
        qpalette.setBrush(
            QtGui.QPalette.Highlight,
            QtGui.QColor(201, 214, 255, 35)
        )
        # apply new palette on the QTextEdit
        parent.setPalette(qpalette)


    def drawDisplay(self, painter, option, rect, text):
        # painter.save()
        rect = option.rect

        self._txt_document.setPlainText(text)
        painter.translate( rect.x(), rect.y() -4)
        ctxt = QtGui.QAbstractTextDocumentLayout.PaintContext()
        self._txt_document.documentLayout().draw(painter, ctxt)
        painter.restore()

    def on_palette_change(self):
        self.needs_rehighlight.emit()

    def rehighlight(self):
        self._highlight.update_styles()
        self._highlight.rehighlight()
        self.parent().repaint(self.parent().visibleRegion())


class QuickHelpHiglighter(QtGui.QSyntaxHighlighter):

    needs_rehighlight = QtCore.Signal()

    line_regex = '(-)(\w+)(/)(-)(\w+)([\s\w|]*)'

    def __init__(self, txt_document, palette):
        super(QuickHelpHiglighter, self).__init__(txt_document)
        self._enabled = True
        self._palette = palette
        self._styles = self._get_styles()
        self._rules = self._get_rules()

    def update_styles(self):
        self._styles = self._get_styles()
        self._rules = self._get_rules()

    def set_enabled(self, state):
        self._enabled = state

    def on_palette_change(self):
        self.needs_rehighlight.emit()

    def highlightBlock(self, line):
        """
        Args:
            line (str)

        Apply syntax highlighting to the given line.
        """

        if not self._enabled:
            return

        # straight-forward regex rules, no block state used
        for regex, nth, txt_format in self._rules:
            self.apply_rule(line, regex, nth, txt_format, [(0, len(line))])

    def _get_rules(self):
        """
        Returns:
            (list[tuple(QtCore.QRegExp, int, QtGui.QTextCharFormat)])
        """

        rules = []

        if 'operators' in self._styles:
            rules += [(self.line_regex, 1, self._styles['operators'])]
            rules += [(self.line_regex, 4, self._styles['operators'])]

        if 'shortname' in self._styles:
            rules += [(self.line_regex, 2, self._styles['shortname'])]
        if 'longname' in self._styles:
            rules += [(self.line_regex, 5, self._styles['longname'])]

        if 'builtins' in self._styles:
            rules += [(self.line_regex, 3, self._styles['builtins'])]
            rules += [(self.line_regex, 6, self._styles['builtins'])]

        return [(Regex(regex), nth, style) for regex, nth, style in rules]

    def apply_rule(self, line, regex, nth, txt_format, ranges):
        """
        Args:
            line (str)
            regex (Regex)
            nth (int) : the nth matching group that is to be highlighted
            txt_format (QtGui.QTextCharFormat)

        Apply <txt_format> on segments that matches <regex> in <line>.
        """

        for rge in ranges:
            segment = line[rge[0]:rge[1]]
            if not segment:
                continue

            for match in regex.finditer(segment):
                start = match.start(nth)
                end = match.end(nth)

                self.setFormat(start +rge[0], end -start, txt_format)

    def _get_styles(self):
        """
        Returns:
            (dict) : from type {'name': QtGui.QTextCharFormat}

        Get text formats by name as dict.
        """

        if self._palette is None:
            return {}

        return self._palette.char_formatted()
