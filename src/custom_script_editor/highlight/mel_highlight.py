"""
Script Editor's Mel tabs (or Console, whenever Mel language is detected) higlight
classes (derivated from ./common.py).
"""

from custom_script_editor import constants as kk
from custom_script_editor.highlight.common import Painter, Highlighter, Regex

try:
    from maya import cmds
    MAYA_CMDS =  list(set(cmds.help('[a-z]*', list=True, lng='mel')))
except ImportError:
    MAYA_CMDS = []


class MelHighlighter(Highlighter):
    """
    Mel highlighter.
    """

    def __init__(self, txt_edit, palette, padding=True):
        super(MelHighlighter, self).__init__(txt_edit, palette, padding)

        self._painter = MelPainter(self, palette)


class MelPainter(Painter):
    """
    Syntax highlighter for MEL tabs.
    """

    docstr_chars = ['/\\*']
    docstr_close_chars = ['\\*/']
    raw_str_chars = []
    raw_str_close_chars = []
    cmnt_chars = ['//']
    str_chars = ['"']

    def _get_rules(self):
        """
        Returns:
            (list[tuple(Regex, int, QtGui.QTextCharFormat)])

        Get all MEL rules, except for comments, strings and triple-quotes,
        that will be handled differently.
        """

        rules = []

        if 'numbers' in self._styles:
            # digits rule
            rules += [('\\b\d+\\b', 0, self._styles['numbers'])]

        if 'called' in self._styles:
            rules += [('^\s*\w+', 0, self._styles['called'])]
            rules += [('\\b(\w+)\s*\(.*\)', 1, self._styles['called'])]

        if MAYA_CMDS and 'maya_cmds' in self._styles:
            rules += [
                ('\\b%s\\b' % x, 0, self._styles['maya_cmds'])
                for x in MAYA_CMDS
            ]

        if 'flags' in self._styles:
            rules += [('-(\w+)', 1, self._styles['flags'])]

        if 'called_expr' in self._styles:
            # expressions between ``
            rules += [('(`.*`)', 1, self._styles['called_expr'])]

        # if 'strings' in self._styles:
        #     rules += [('(\"\w*\")', 1, self._styles['strings'])]

        if 'variables' in self._styles:
            # $variables rules
            rules += [('\$\w+', 0, self._styles['variables'])]

        if 'keywords' in self._styles:
            # add MEL keywords rules
            rules += [('\\b(%s)\\b' % w, 0, self._styles['keywords']) for w in kk.MEL_KEYWORDS]
        if 'mel_numbers' in self._styles:
            # add MEL numbers rules
            rules += [('\\b(%s)\\b' % n, 0, self._styles['mel_numbers']) for n in kk.MEL_NUMBERS]
        if 'builtins' in self._styles:
            # add MEL builtins rules
            rules += [('\\b(%s)\\b' % n, 0, self._styles['builtins']) for n in kk.MEL_BUILTINS]
        if 'operators' in self._styles:
            # add operators rules
            rules += [('%s' % o, 0, self._styles['operators']) for o in kk.OPERATORS]

        if 'proc_names' in self._styles:
            # declared procedures rule
            rules += [ ('(\\bproc\\b\s+)(.+\s+)*(\w+)\s*\(', 3, self._styles['proc_names'])]

        if 'numbers' in self._styles:
            # numeric literals
            rules += [
                (r'\b[+-]?[0-9]+[lL]?\b', 0, self._styles['numbers']),
                (r'\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\b', 0, self._styles['numbers']),
                (r'\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b', 0, self._styles['numbers'])
            ]

        return [(Regex(regex), nth, style) for regex, nth, style in rules]
