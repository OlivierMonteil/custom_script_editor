"""
Data containers for highlight palettes and profiles.
"""

import copy

from PySide2 import QtCore, QtGui

from custom_script_editor.prefs import data


class HighlightData(QtCore.QObject):
    """
    Profiles management class, kind of a palette-data filter before highlithers.
    """

    data_changed = QtCore.Signal()

    def __init__(self, parent):
        super(HighlightData, self).__init__(parent=parent)

        self._data = {}

    def set(self, data):
        """
        Args:
            attr (str)

        Set profile's data.
        """

        self._data = dict(data)
        self.data_changed.emit()

    def get(self):
        """
        Returns:
            (dict)

        Get profile's data (by copy).
        """

        return dict(self._data)

    def get_attr(self, attr):
        """
        Args:
            attr (str)

        Returns:
            (bool)

        Get <attr>'s enabled state from profile's data.
        """

        value = self._data[attr]

        # return by copy
        if isinstance(value, dict):
            return dict(value)
        if isinstance(value, tuple):
            return tuple(value)
        if isinstance(value, list):
            return list(value)

        return value

    def set_attr(self, attr, value):
        """
        Args:
            attr (str)

        Set <attr>'s enabled state into profile's data.
        """

        self._data[attr] = value
        self.data_changed.emit()


class Palette(HighlightData):
    """
    Palettes data container, holds its own Profile instance.
    """

    specific_formats = {}

    def __init__(self, parent=None, palette_data=None, profile_data=None):
        super(Palette, self).__init__(parent)

        self._type = None
        self._profile = HighlightData(self)

        # set startup data
        if not palette_data is None:
            self.set(palette_data)
        # set startup profile
        if not profile_data is None:
            self._set_profile_data(profile_data)

        self._profile.data_changed.connect(self.data_changed.emit)

    def _set_profile_data(self, data_dict):
        self._profile.set(data_dict)

    def set_profile_by_name(self, name):
        self._set_profile_data(data.get_profile_data(self._type, name))

    def get_type(self):
        return self._type

    def profile(self):
        """
        Returns:
            (HighlightData)

        Get profile's data containter object.
        """

        return self._profile

    def get_profile_applied_data(self):
        """
        Returns:
            (dict)

        Get data's copy with enabled states edited from profile.
        """

        data_copy = copy.deepcopy(self._data)
        profile_data = self._profile.get()

        for attr in data_copy or ():
            attr_dict = data_copy[attr]
            # skip new attributes that would not been added to profile files
            if not attr in profile_data:
                continue
            # profile is None, no edit
            if profile_data[attr] is None:
                continue
            # default attributes like "background" have no "enabled" attribute
            if not 'enabled' in attr_dict:
                continue
            # edit enabled state from profile's
            data_copy[attr]['enabled'] = profile_data[attr]

        return data_copy

    def char_formatted(self):
        """
        Returns:
            (dict)

        Get Palette data as {'attribute': QtGui.QTextCharFormat} dict, applying
        profile and replacing linked values by their source's.
        """

        result_dict = {}
        profiled_data = self.get_profile_applied_data()

        for attr in profiled_data or ():
            attr_dict = profiled_data[attr]

            # simply do not add disabled attributes to the returned dict
            if 'enabled' in attr_dict and not attr_dict['enabled']:
                continue

            # handle "italic", "bold", etc. specific formats
            if self.specific_formats and attr in self.specific_formats:
                result_dict[attr] = char_format(
                    get_linked_value(profiled_data, attr),
                    self.specific_formats[attr]
                )

            else:
                result_dict[attr] = char_format(
                    get_linked_value(profiled_data, attr)
                )

        return result_dict

    def get_color(self, attr):
        """
        Args:
            attr (str)

        Get dict's color at <attr> key.
        """

        obj = self._data[attr]

        return tuple(obj["value"]) if isinstance(obj, dict) else tuple(obj)

    def set_color(self, attr, rgb):
        """
        Args:
            attr (str)
            rgb (tuple or list)

        Get dict's color at <attr> key.
        """

        obj = self._data[attr]
        value = tuple(rgb)

        if isinstance(obj, dict):
            self._data[attr]['value'] = value
        else:
            self._data[attr] = value
        self.data_changed.emit()


class PythonPalette(Palette):

    specific_formats = {
        'def_names' : 'bold',
        'class_names' : 'bold',
        'comments' : 'italic',
    }

    def __init__(self, *args, **kwargs):
        super(PythonPalette, self).__init__(*args, **kwargs)

        self._type = 'python'


class MelPalette(Palette):

    specific_formats = {
        'def_names' : 'bold',
        'class_names' : 'bold',
        'comments' : 'italic',
    }

    def __init__(self, *args, **kwargs):
        super(MelPalette, self).__init__(*args, **kwargs)

        self._type = 'mel'


class ConsolePalette(Palette):

    specific_formats = {
        'warning' : 'italic',
        'error' : 'bold',
        'success' : 'bold',
    }

    def __init__(self, *args, **kwargs):
        super(ConsolePalette, self).__init__(*args, **kwargs)

        self._type = 'console'


class QuickHelpPalette(Palette):

    def __init__(self, *args, **kwargs):
        super(QuickHelpPalette, self).__init__(*args, **kwargs)

        self._type = 'quick-help'


def char_format(rgb, style=''):
    """
    Args:
        rgb (tuple(int))
        style (str, optional)

    Returns:
        (QtGui.QTextCharFormat)

    Return a QtGui.QTextCharFormat with the given attributes (color, font
    weigth, etc).
    """

    if isinstance(rgb, tuple):
        color = QtGui.QColor(*rgb)
    else:
        # handle named colors as "red"
        color = QtGui.QColor()
        color.setNamedColor(rgb)

    ch_format = QtGui.QTextCharFormat()
    ch_format.setForeground(color)

    if 'bold' in style:
        ch_format.setFontWeight(QtGui.QFont.Bold)
    if 'italic' in style:
        ch_format.setFontItalic(True)

    return ch_format


def get_linked_value(profiled_data, attr):
    """
    Args:
        profiled_data (dict)
        attr (str)

    Returns:
        (tuple(float))

    Recursively get linked color for attribute (if has no link, just get its
    color value).
    """

    attr_dict = profiled_data[attr]

    # no link, simply get its color value
    if not 'link' in attr_dict or attr_dict['link'] is None:
        return tuple(attr_dict['value'])

    linked_attr = attr_dict['link']

    # link not found into dict (for security, should not be necessary)
    if not linked_attr in profiled_data:
        return tuple(attr_dict['value'])

    # recurse through linked attribute
    return get_linked_value(profiled_data, linked_attr)
