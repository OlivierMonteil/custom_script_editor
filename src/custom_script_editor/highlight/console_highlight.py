"""
Script Editor's Console higlight classes (derivated from ./common.py).
"""

from PySide2 import QtGui

from custom_script_editor.highlight.common import Painter, Highlighter, Regex
from custom_script_editor.highlight.mel_highlight import MelPainter
from custom_script_editor.highlight.python_highlight import PythonPainter
from custom_script_editor.highlight.language import Detector
from custom_script_editor import constants as kk


BASE_MESSAGES = ['error', 'success', 'warning', 'info']
TRACEBACK_START_REGEX = Regex('^(#\s*)*Traceback.*')
TRACEBACK_REGEX = Regex('^(#\s)*\s+.*')

MAX_LINE_COUNT = 1000


class ConsoleHighlighter(Highlighter):
    """
    Console highlighter.
    """

    def __init__(self, txt_edit, palette, mel_palette, python_palette, padding=False):
        super(ConsoleHighlighter, self).__init__(txt_edit, palette, padding)

        self._painter = ConsolePainter(self, palette, mel_palette, python_palette)

    def rehighlight(self):
        """
        Qt re-implementation.
        Do not paint mel/python commands at re-highlight, which is called at
        Script Editor's opening : avoid long computation on re-opening after a
        long session and a very long console content to highlight.
        """

        if self._txt_edit.document().blockCount() > MAX_LINE_COUNT:
            self._painter.set_messages_only(True)

        super(ConsoleHighlighter, self).rehighlight()

        self._painter.set_messages_only(False)

    def highlightBlock(self, line):
        """
        Args:
            line (str)

        Qt re-implementation. Apply syntax highlighting to the given line using
        the associated Painter object.
        """

        if not hasattr(self, '_painter'):
            self.corrupted.emit()
            return

        self._painter.paint(line)

    def reset(self, txt_edit, palette, mel_palette, python_palette, padding=False):
        """
        Args:
            palette (ConsolePalette)
            mel_palette (MelPalette)
            python_palette (PythonPalette)

        For some reason, ConsoleHighlighter may lose all its attributes. In this
        case, it will emit "corrupted" QtCore.Signal(), and we need to reset its
        attributes.

        Obviously this is not a very elegant way of fixing this issue, so
        fixing the source of this problem should be investigated...
        """

        self._painter = ConsolePainter(self, palette, mel_palette, python_palette)
        self._txt_edit = txt_edit
        self._palette = palette
        self._padding = padding
        self._enabled = True

        self._set_style_sheet()


class ConsolePainter(Painter):
    """
    Syntax highlight painter for the Console panel (MEL/Python/console).
    """

    def __init__(self, highlighter, palette, mel_palette, python_palette):

        self._current_rule = 'console'
        self._messages_only = False

        self._mel_painter = MelPainter(highlighter, mel_palette)
        self._python_painter = PythonPainter(highlighter, python_palette)
        self._language_detector = Detector()

        super(ConsolePainter, self).__init__(highlighter, palette)

        self._blocking_rules = self._get_blocking_rules()

    def update(self):
        """ Painter re-implementation, also update Mel and Python painters. """

        self._mel_painter.update()
        self._python_painter.update()

        self._styles = self._get_styles()
        self._rules = self._get_rules()
        self._blocking_rules = self._get_blocking_rules()

    def paint(self, line):
        """
        Args:
            line (str)

        Apply syntax highlighting rules to the given line.
        """

        # propagate previous line's state
        self.set_current_block_state(self.previous_block_state())

        try:
            if self._traceback_applied(line):
                return

            # apply blocking rules
            for regex, txt_format in self._blocking_rules:
                match = regex.search(line)

                if match:
                    # set format only if rule is enabled
                    # (see self._get_blocking_rules docstring)
                    if not txt_format is None:
                        self.set_format(0, len(line), txt_format)
                    self._current_rule = 'console'

                    # self.set_current_block_state(-1)
                    return

            if self._messages_only:
                return

            # do not change language if some current block state is opened
            if self.current_block_state() != -1:
                if self._current_rule == 'mel':
                    self._mel_painter.paint(line)
                    return

                if self._current_rule == 'python':
                    self._python_painter.paint(line)
                    return

            language = self._language_detector.get_language(line)

            if language == 'mel':
                self._current_rule = 'mel'
                self._mel_painter.paint(line)

            elif language == 'python':
                self._current_rule = 'python'
                self._python_painter.paint(line)

            else:
                for regex, nth, txt_format in self._rules or ():
                    self.apply_rule(line, regex, nth, txt_format, [(0, len(line))])

        # silent errors so we don't fall into a print loop...
        except:
            pass

    def _get_rules(self):
        """
        Returns:
            (list[tuple(QtCore.QRegExp, int, QtGui.QTextCharFormat)])

        These rules are applied prior to MEL and Python rules. If any rule was
        used, all MEL, Python and self rules will be ignored.
        """

        rules = []

        if 'python_modules' in self._styles:
            # printed Python modules or methods (like <module 'maya' from '...'>)
            # rule, if not in string.
            rules = [
                (
                    '(?<![\"\'])(<\s*\w+\s+\'.+\'\s+from\s+\'.+\'>)(?![\"\'])',
                    1,
                    self._styles['python_modules']
                )
            ]

        if 'python_objects' in self._styles:
            # printed Python objects (like <PySide2.QtWidgets.QWidget>) rule if
            # not in string.
            rules += [
                (
                    '(?<![\"\'])(<\s*.+\s+object at\s+.+>\s*)(?![\"\'])',
                    1,
                    self._styles['python_objects']
                )
            ]

        return [(Regex(regex), nth, style) for regex, nth, style in rules]

    def _get_blocking_rules(self):
        """
        Returns:
            (list[tuple(QtCore.QRegExp, QtGui.QTextCharFormat)])

        Get all message rules. These are applied on the whole line, analysing the
        line with no case match.
        Here, all rules will returned, whether they are enabled or not, so mel
        or python highlight won't be applied instead in matching lines.

        --> self.get_style_by_name will return None for disabled rules (not
        found into self._styles), and matches with no style will not set any
        format on text but will behave as usual in terms of passing/blocking
        next rules evaluation.
        """

        rules = []

        for msg in BASE_MESSAGES:
            msg_style = self.get_style_by_name(msg)

            # set info, warning, error and success messages rules
            for char in ('//', '#'):
                # set rules for all string case
                for x in (msg, msg.upper(), msg.capitalize()):
                    rules += [
                        (
                            '^\s*%(char)s(.)+(%(msg)s\s*[:\|])' % {'char': char, 'msg': x},
                            msg_style
                        )
                    ]

            # info lines with '[] msg:' at the start of the line
            rules += [('^\s*\[\w+\]\s*(%s\s*:)' % msg, msg_style)]

        warning_style = self.get_style_by_name('warning')
        rules += [('(Internal C\+\+ object .+ already deleted\.)', warning_style)]

        info_style = self.get_style_by_name('info')
        # info lines with '//' or '#' at the start of the line
        rules += [('^\s*%s.*' % c, info_style) for c in ('//', '#')]
        # info lines with '//' or '#' at the end of the line
        rules += [('.*%s\s*$' % c, info_style) for c in ('//', '#')]

        return [(Regex(regex), style) for regex, style in rules]

    def _traceback_applied(self, line):
        """
        Args:
            line (str)

        Returns:
            (bool) : whether any traceback has been detected (see self.paint())

        Handle Tracebacks (block state = 5).
        """

        if 'traceback' in self._styles and TRACEBACK_START_REGEX.match(line):
            self.set_current_block_state(5)
            self.set_format(0, len(line), self._styles['traceback'])
            return True

        if self.current_block_state() == 5:
            if 'traceback' in self._styles and TRACEBACK_REGEX.match(line):
                self.set_format(0, len(line), self._styles['traceback'])
                return True
            else:
                self.set_current_block_state(-1)

        return False

    def set_messages_only(self, state):
        self._messages_only = state
