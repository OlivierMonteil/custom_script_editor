"""
Maya's Script Editor's QTextEdit instances highlighting package.
Handles Mel/Python tabs and console highlight.

The custom_script_editor.managers.HighlightManager class is used to add this
system to the Script Editor's QTextEdit instances.
"""

# shorten absolute imports
from custom_script_editor.highlight.console_highlight import ConsoleHighlighter
from custom_script_editor.highlight.mel_highlight import MelHighlighter
from custom_script_editor.highlight.python_highlight import PythonHighlighter
from custom_script_editor.highlight.quick_help import QuickHelpDelegate
from custom_script_editor.highlight.quick_help import QuickHelpHiglighter
from custom_script_editor.highlight.common import Regex, STYLE_PATTERN
from custom_script_editor.highlight.palettes import char_format
from custom_script_editor.highlight.palettes import ConsolePalette
from custom_script_editor.highlight.palettes import PythonPalette
from custom_script_editor.highlight.palettes import MelPalette
from custom_script_editor.highlight.palettes import QuickHelpPalette
