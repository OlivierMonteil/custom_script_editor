.. Custom Script Editor master file, created by
   sphinx-quickstart on Fri Oct  2 23:43:00 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Custom Script Editor's code documentation!
=====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Core Modules
============

main.py
-------

.. automodule:: custom_script_editor.main

managers.py
-----------

.. automodule:: custom_script_editor.managers

overlay.py
----------

.. automodule:: custom_script_editor.overlay

menu.py
-------

.. automodule:: custom_script_editor.menu

utils.py
--------

.. automodule:: custom_script_editor.utils

constants.py
------------

.. automodule:: custom_script_editor.constants

controls.py
-----------

.. automodule:: custom_script_editor.controls

dialogs.py
----------

.. automodule:: custom_script_editor.dialogs

objects_inspector.py
--------------------

.. automodule:: custom_script_editor.objects_inspector


Highlight
=========

common.py
---------

.. automodule:: custom_script_editor.highlight.common

palettes.py
-----------

.. automodule:: custom_script_editor.highlight.palettes

language.py
-----------

.. automodule:: custom_script_editor.highlight.language

console_highlight.py
--------------------

.. automodule:: custom_script_editor.highlight.console_highlight

mel_highlight.py
----------------

.. automodule:: custom_script_editor.highlight.mel_highlight

python_highlight.py
-------------------

.. automodule:: custom_script_editor.highlight.python_highlight


Multi-cursor
============

handler.py
----------

.. automodule:: custom_script_editor.multi_cursor.handler

text_cursor.py
--------------

.. automodule:: custom_script_editor.multi_cursor.text_cursor

Blocks-collapse
===============

handler.py
----------

.. automodule:: custom_script_editor.blocks_collapse.handler

common.py
---------

.. automodule:: custom_script_editor.blocks_collapse.common

collapse_widget.py
------------------

.. automodule:: custom_script_editor.blocks_collapse.collapse_widget

line_numbers.py
---------------

.. automodule:: custom_script_editor.blocks_collapse.line_numbers

Keys
====

handler.py
----------

.. automodule:: custom_script_editor.keys.handler

Snippets (auto-completion)
==========================

common.py
---------

.. automodule:: custom_script_editor.snippets.common

python_snippets.py
------------------

.. automodule:: custom_script_editor.snippets.python_snippets

mel_snippets.py
---------------

.. automodule:: custom_script_editor.snippets.mel_snippets

Prefs
=====

data.py
-------

.. automodule:: custom_script_editor.prefs.data

prefs_editor.py
---------------

.. automodule:: custom_script_editor.prefs.prefs_editor

palette_editor.py
-----------------

.. automodule:: custom_script_editor.prefs.palette_editor

Stylesheet
==========

handler.py
----------

.. automodule:: custom_script_editor.stylesheet.handler


Tools
=====

menu.py
-------

.. automodule:: custom_script_editor.tools.menu

dirs_navigator.py
-----------------

.. automodule:: custom_script_editor.tools.dirs_navigator
